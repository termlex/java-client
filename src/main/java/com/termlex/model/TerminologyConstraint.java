/**
 * Termlex API
 * Termlex is a RESTful, service oriented terminology server and SNOMED CT API
 *
 * OpenAPI spec version: 3.2.0
 * Contact: info@noesisinformatica.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.termlex.model;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import com.termlex.model.ConstraintValueOfobject;
import com.termlex.model.Expression;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * TerminologyConstraint
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2019-09-24T11:39:48.472+01:00")
public class TerminologyConstraint   {
  @SerializedName("cgf")
  private String cgf = null;

  /**
   * Gets or Sets dimensionVocabulary
   */
  public enum DimensionVocabularyEnum {
    @SerializedName("SECOND")
    SECOND("SECOND"),
    
    @SerializedName("HOUR")
    HOUR("HOUR"),
    
    @SerializedName("DAY")
    DAY("DAY"),
    
    @SerializedName("WEEK")
    WEEK("WEEK"),
    
    @SerializedName("MONTH")
    MONTH("MONTH"),
    
    @SerializedName("YEAR")
    YEAR("YEAR"),
    
    @SerializedName("NULL")
    NULL("NULL"),
    
    @SerializedName("DATE")
    DATE("DATE");

    private String value;

    DimensionVocabularyEnum(String value) {
      this.value = value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }
  }

  @SerializedName("dimensionVocabulary")
  private DimensionVocabularyEnum dimensionVocabulary = null;

  @SerializedName("expression")
  private Expression expression = null;

  @SerializedName("expressionUuid")
  private String expressionUuid = null;

  @SerializedName("id")
  private String id = null;

  @SerializedName("name")
  private String name = null;

  @SerializedName("precoordinated")
  private Boolean precoordinated = null;

  /**
   * Gets or Sets subsumption
   */
  public enum SubsumptionEnum {
    @SerializedName("SELF_OR_ANY_TYPE_OF")
    SELF_OR_ANY_TYPE_OF("SELF_OR_ANY_TYPE_OF"),
    
    @SerializedName("SELF_ONLY")
    SELF_ONLY("SELF_ONLY"),
    
    @SerializedName("ANY_TYPE_OF_BUT_NOT_SELF")
    ANY_TYPE_OF_BUT_NOT_SELF("ANY_TYPE_OF_BUT_NOT_SELF"),
    
    @SerializedName("NOT_SELF")
    NOT_SELF("NOT_SELF"),
    
    @SerializedName("NOT_SELF_OR_ANY_TYPE_OF")
    NOT_SELF_OR_ANY_TYPE_OF("NOT_SELF_OR_ANY_TYPE_OF"),
    
    @SerializedName("UNKNOWN")
    UNKNOWN("UNKNOWN");

    private String value;

    SubsumptionEnum(String value) {
      this.value = value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }
  }

  @SerializedName("subsumption")
  private SubsumptionEnum subsumption = null;

  /**
   * Gets or Sets type
   */
  public enum TypeEnum {
    @SerializedName("DATA")
    DATA("DATA"),
    
    @SerializedName("TEMPORAL")
    TEMPORAL("TEMPORAL"),
    
    @SerializedName("TERMINOLOGY")
    TERMINOLOGY("TERMINOLOGY");

    private String value;

    TypeEnum(String value) {
      this.value = value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }
  }

  @SerializedName("type")
  private TypeEnum type = null;

  @SerializedName("uuid")
  private String uuid = null;

  @SerializedName("value")
  private ConstraintValueOfobject value = null;

  public TerminologyConstraint cgf(String cgf) {
    this.cgf = cgf;
    return this;
  }

   /**
   * Get cgf
   * @return cgf
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getCgf() {
    return cgf;
  }

  public void setCgf(String cgf) {
    this.cgf = cgf;
  }

  public TerminologyConstraint dimensionVocabulary(DimensionVocabularyEnum dimensionVocabulary) {
    this.dimensionVocabulary = dimensionVocabulary;
    return this;
  }

   /**
   * Get dimensionVocabulary
   * @return dimensionVocabulary
  **/
  @ApiModelProperty(example = "null", value = "")
  public DimensionVocabularyEnum getDimensionVocabulary() {
    return dimensionVocabulary;
  }

  public void setDimensionVocabulary(DimensionVocabularyEnum dimensionVocabulary) {
    this.dimensionVocabulary = dimensionVocabulary;
  }

  public TerminologyConstraint expression(Expression expression) {
    this.expression = expression;
    return this;
  }

   /**
   * Get expression
   * @return expression
  **/
  @ApiModelProperty(example = "null", value = "")
  public Expression getExpression() {
    return expression;
  }

  public void setExpression(Expression expression) {
    this.expression = expression;
  }

  public TerminologyConstraint expressionUuid(String expressionUuid) {
    this.expressionUuid = expressionUuid;
    return this;
  }

   /**
   * Get expressionUuid
   * @return expressionUuid
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getExpressionUuid() {
    return expressionUuid;
  }

  public void setExpressionUuid(String expressionUuid) {
    this.expressionUuid = expressionUuid;
  }

  public TerminologyConstraint id(String id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public TerminologyConstraint name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public TerminologyConstraint precoordinated(Boolean precoordinated) {
    this.precoordinated = precoordinated;
    return this;
  }

   /**
   * Get precoordinated
   * @return precoordinated
  **/
  @ApiModelProperty(example = "null", value = "")
  public Boolean getPrecoordinated() {
    return precoordinated;
  }

  public void setPrecoordinated(Boolean precoordinated) {
    this.precoordinated = precoordinated;
  }

  public TerminologyConstraint subsumption(SubsumptionEnum subsumption) {
    this.subsumption = subsumption;
    return this;
  }

   /**
   * Get subsumption
   * @return subsumption
  **/
  @ApiModelProperty(example = "null", value = "")
  public SubsumptionEnum getSubsumption() {
    return subsumption;
  }

  public void setSubsumption(SubsumptionEnum subsumption) {
    this.subsumption = subsumption;
  }

  public TerminologyConstraint type(TypeEnum type) {
    this.type = type;
    return this;
  }

   /**
   * Get type
   * @return type
  **/
  @ApiModelProperty(example = "null", value = "")
  public TypeEnum getType() {
    return type;
  }

  public void setType(TypeEnum type) {
    this.type = type;
  }

  public TerminologyConstraint uuid(String uuid) {
    this.uuid = uuid;
    return this;
  }

   /**
   * Get uuid
   * @return uuid
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public TerminologyConstraint value(ConstraintValueOfobject value) {
    this.value = value;
    return this;
  }

   /**
   * Get value
   * @return value
  **/
  @ApiModelProperty(example = "null", value = "")
  public ConstraintValueOfobject getValue() {
    return value;
  }

  public void setValue(ConstraintValueOfobject value) {
    this.value = value;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TerminologyConstraint terminologyConstraint = (TerminologyConstraint) o;
    return Objects.equals(this.cgf, terminologyConstraint.cgf) &&
        Objects.equals(this.dimensionVocabulary, terminologyConstraint.dimensionVocabulary) &&
        Objects.equals(this.expression, terminologyConstraint.expression) &&
        Objects.equals(this.expressionUuid, terminologyConstraint.expressionUuid) &&
        Objects.equals(this.id, terminologyConstraint.id) &&
        Objects.equals(this.name, terminologyConstraint.name) &&
        Objects.equals(this.precoordinated, terminologyConstraint.precoordinated) &&
        Objects.equals(this.subsumption, terminologyConstraint.subsumption) &&
        Objects.equals(this.type, terminologyConstraint.type) &&
        Objects.equals(this.uuid, terminologyConstraint.uuid) &&
        Objects.equals(this.value, terminologyConstraint.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(cgf, dimensionVocabulary, expression, expressionUuid, id, name, precoordinated, subsumption, type, uuid, value);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TerminologyConstraint {\n");
    
    sb.append("    cgf: ").append(toIndentedString(cgf)).append("\n");
    sb.append("    dimensionVocabulary: ").append(toIndentedString(dimensionVocabulary)).append("\n");
    sb.append("    expression: ").append(toIndentedString(expression)).append("\n");
    sb.append("    expressionUuid: ").append(toIndentedString(expressionUuid)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    precoordinated: ").append(toIndentedString(precoordinated)).append("\n");
    sb.append("    subsumption: ").append(toIndentedString(subsumption)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    uuid: ").append(toIndentedString(uuid)).append("\n");
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

