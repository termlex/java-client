/**
 * Termlex API
 * Termlex is a RESTful, service oriented terminology server and SNOMED CT API
 *
 * OpenAPI spec version: 3.2.0
 * Contact: info@noesisinformatica.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.termlex.model;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import com.termlex.model.Organisation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.OffsetDateTime;


/**
 * CodeSystem
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2019-09-24T11:39:48.472+01:00")
public class CodeSystem   {
  @SerializedName("caseSensitive")
  private Boolean caseSensitive = null;

  @SerializedName("copyright")
  private String copyright = null;

  @SerializedName("createdTime")
  private OffsetDateTime createdTime = null;

  @SerializedName("date")
  private OffsetDateTime date = null;

  @SerializedName("id")
  private String id = null;

  @SerializedName("inline")
  private Boolean inline = null;

  @SerializedName("name")
  private String name = null;

  @SerializedName("ownerId")
  private String ownerId = null;

  @SerializedName("publisher")
  private Organisation publisher = null;

  @SerializedName("shortName")
  private String shortName = null;

  /**
   * Gets or Sets status
   */
  public enum StatusEnum {
    @SerializedName("DRAFT")
    DRAFT("DRAFT"),
    
    @SerializedName("ACTIVE")
    ACTIVE("ACTIVE"),
    
    @SerializedName("RETIRED")
    RETIRED("RETIRED");

    private String value;

    StatusEnum(String value) {
      this.value = value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }
  }

  @SerializedName("status")
  private StatusEnum status = null;

  @SerializedName("system")
  private String system = null;

  /**
   * Gets or Sets type
   */
  public enum TypeEnum {
    @SerializedName("SCT")
    SCT("SCT"),
    
    @SerializedName("SCT_RF2")
    SCT_RF2("SCT_RF2"),
    
    @SerializedName("SCT_RF1")
    SCT_RF1("SCT_RF1"),
    
    @SerializedName("ICD")
    ICD("ICD"),
    
    @SerializedName("LOINC")
    LOINC("LOINC"),
    
    @SerializedName("DMD")
    DMD("DMD"),
    
    @SerializedName("ACT")
    ACT("ACT"),
    
    @SerializedName("NICIP")
    NICIP("NICIP"),
    
    @SerializedName("READ")
    READ("READ"),
    
    @SerializedName("CTV")
    CTV("CTV"),
    
    @SerializedName("REFSET")
    REFSET("REFSET"),
    
    @SerializedName("ICD_O")
    ICD_O("ICD_O"),
    
    @SerializedName("VALUESET")
    VALUESET("VALUESET");

    private String value;

    TypeEnum(String value) {
      this.value = value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }
  }

  @SerializedName("type")
  private TypeEnum type = null;

  @SerializedName("updatedBy")
  private String updatedBy = null;

  public CodeSystem caseSensitive(Boolean caseSensitive) {
    this.caseSensitive = caseSensitive;
    return this;
  }

   /**
   * Get caseSensitive
   * @return caseSensitive
  **/
  @ApiModelProperty(example = "null", value = "")
  public Boolean getCaseSensitive() {
    return caseSensitive;
  }

  public void setCaseSensitive(Boolean caseSensitive) {
    this.caseSensitive = caseSensitive;
  }

  public CodeSystem copyright(String copyright) {
    this.copyright = copyright;
    return this;
  }

   /**
   * Get copyright
   * @return copyright
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getCopyright() {
    return copyright;
  }

  public void setCopyright(String copyright) {
    this.copyright = copyright;
  }

  public CodeSystem createdTime(OffsetDateTime createdTime) {
    this.createdTime = createdTime;
    return this;
  }

   /**
   * Get createdTime
   * @return createdTime
  **/
  @ApiModelProperty(example = "null", value = "")
  public OffsetDateTime getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(OffsetDateTime createdTime) {
    this.createdTime = createdTime;
  }

  public CodeSystem date(OffsetDateTime date) {
    this.date = date;
    return this;
  }

   /**
   * Get date
   * @return date
  **/
  @ApiModelProperty(example = "null", value = "")
  public OffsetDateTime getDate() {
    return date;
  }

  public void setDate(OffsetDateTime date) {
    this.date = date;
  }

  public CodeSystem id(String id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public CodeSystem inline(Boolean inline) {
    this.inline = inline;
    return this;
  }

   /**
   * Get inline
   * @return inline
  **/
  @ApiModelProperty(example = "null", value = "")
  public Boolean getInline() {
    return inline;
  }

  public void setInline(Boolean inline) {
    this.inline = inline;
  }

  public CodeSystem name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public CodeSystem ownerId(String ownerId) {
    this.ownerId = ownerId;
    return this;
  }

   /**
   * Get ownerId
   * @return ownerId
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getOwnerId() {
    return ownerId;
  }

  public void setOwnerId(String ownerId) {
    this.ownerId = ownerId;
  }

  public CodeSystem publisher(Organisation publisher) {
    this.publisher = publisher;
    return this;
  }

   /**
   * Get publisher
   * @return publisher
  **/
  @ApiModelProperty(example = "null", value = "")
  public Organisation getPublisher() {
    return publisher;
  }

  public void setPublisher(Organisation publisher) {
    this.publisher = publisher;
  }

  public CodeSystem shortName(String shortName) {
    this.shortName = shortName;
    return this;
  }

   /**
   * Get shortName
   * @return shortName
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getShortName() {
    return shortName;
  }

  public void setShortName(String shortName) {
    this.shortName = shortName;
  }

  public CodeSystem status(StatusEnum status) {
    this.status = status;
    return this;
  }

   /**
   * Get status
   * @return status
  **/
  @ApiModelProperty(example = "null", value = "")
  public StatusEnum getStatus() {
    return status;
  }

  public void setStatus(StatusEnum status) {
    this.status = status;
  }

  public CodeSystem system(String system) {
    this.system = system;
    return this;
  }

   /**
   * Get system
   * @return system
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getSystem() {
    return system;
  }

  public void setSystem(String system) {
    this.system = system;
  }

  public CodeSystem type(TypeEnum type) {
    this.type = type;
    return this;
  }

   /**
   * Get type
   * @return type
  **/
  @ApiModelProperty(example = "null", value = "")
  public TypeEnum getType() {
    return type;
  }

  public void setType(TypeEnum type) {
    this.type = type;
  }

  public CodeSystem updatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
    return this;
  }

   /**
   * Get updatedBy
   * @return updatedBy
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CodeSystem codeSystem = (CodeSystem) o;
    return Objects.equals(this.caseSensitive, codeSystem.caseSensitive) &&
        Objects.equals(this.copyright, codeSystem.copyright) &&
        Objects.equals(this.createdTime, codeSystem.createdTime) &&
        Objects.equals(this.date, codeSystem.date) &&
        Objects.equals(this.id, codeSystem.id) &&
        Objects.equals(this.inline, codeSystem.inline) &&
        Objects.equals(this.name, codeSystem.name) &&
        Objects.equals(this.ownerId, codeSystem.ownerId) &&
        Objects.equals(this.publisher, codeSystem.publisher) &&
        Objects.equals(this.shortName, codeSystem.shortName) &&
        Objects.equals(this.status, codeSystem.status) &&
        Objects.equals(this.system, codeSystem.system) &&
        Objects.equals(this.type, codeSystem.type) &&
        Objects.equals(this.updatedBy, codeSystem.updatedBy);
  }

  @Override
  public int hashCode() {
    return Objects.hash(caseSensitive, copyright, createdTime, date, id, inline, name, ownerId, publisher, shortName, status, system, type, updatedBy);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CodeSystem {\n");
    
    sb.append("    caseSensitive: ").append(toIndentedString(caseSensitive)).append("\n");
    sb.append("    copyright: ").append(toIndentedString(copyright)).append("\n");
    sb.append("    createdTime: ").append(toIndentedString(createdTime)).append("\n");
    sb.append("    date: ").append(toIndentedString(date)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    inline: ").append(toIndentedString(inline)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    ownerId: ").append(toIndentedString(ownerId)).append("\n");
    sb.append("    publisher: ").append(toIndentedString(publisher)).append("\n");
    sb.append("    shortName: ").append(toIndentedString(shortName)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    system: ").append(toIndentedString(system)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    updatedBy: ").append(toIndentedString(updatedBy)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

