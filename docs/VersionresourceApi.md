# VersionresourceApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addConceptsToVersionUsingPOST**](VersionresourceApi.md#addConceptsToVersionUsingPOST) | **POST** /api/versions/{id}/import | addConceptsToVersion
[**createVersionUsingPOST**](VersionresourceApi.md#createVersionUsingPOST) | **POST** /api/versions | createVersion
[**deleteVersionUsingDELETE**](VersionresourceApi.md#deleteVersionUsingDELETE) | **DELETE** /api/versions/{id} | deleteVersion
[**getAllVersionsUsingGET**](VersionresourceApi.md#getAllVersionsUsingGET) | **GET** /api/versions | getAllVersions
[**getConceptsForVersionUsingGET**](VersionresourceApi.md#getConceptsForVersionUsingGET) | **GET** /api/versions/{id}/concepts | getConceptsForVersion
[**getDefaultVersionUsingGET**](VersionresourceApi.md#getDefaultVersionUsingGET) | **GET** /api/versions/default | getDefaultVersion
[**getVersionUsingGET**](VersionresourceApi.md#getVersionUsingGET) | **GET** /api/versions/{id} | getVersion
[**setDefaultVersionUsingPUT**](VersionresourceApi.md#setDefaultVersionUsingPUT) | **PUT** /api/versions/default | setDefaultVersion
[**updateVersionUsingPUT**](VersionresourceApi.md#updateVersionUsingPUT) | **PUT** /api/versions | updateVersion


<a name="addConceptsToVersionUsingPOST"></a>
# **addConceptsToVersionUsingPOST**
> List&lt;TerminologyConceptImpl&gt; addConceptsToVersionUsingPOST(id, concepts)

addConceptsToVersion

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.VersionresourceApi;


VersionresourceApi apiInstance = new VersionresourceApi();
String id = "id_example"; // String | The version of the terminology product as date string
List<Map> concepts = Arrays.asList(new Map()); // List<Map> | concepts
try {
    List<TerminologyConceptImpl> result = apiInstance.addConceptsToVersionUsingPOST(id, concepts);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VersionresourceApi#addConceptsToVersionUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| The version of the terminology product as date string |
 **concepts** | [**List&lt;Map&gt;**](Map.md)| concepts |

### Return type

[**List&lt;TerminologyConceptImpl&gt;**](TerminologyConceptImpl.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createVersionUsingPOST"></a>
# **createVersionUsingPOST**
> Version createVersionUsingPOST(version)

createVersion

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.VersionresourceApi;


VersionresourceApi apiInstance = new VersionresourceApi();
Version version = new Version(); // Version | version
try {
    Version result = apiInstance.createVersionUsingPOST(version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VersionresourceApi#createVersionUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **version** | [**Version**](Version.md)| version |

### Return type

[**Version**](Version.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteVersionUsingDELETE"></a>
# **deleteVersionUsingDELETE**
> deleteVersionUsingDELETE(id)

deleteVersion

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.VersionresourceApi;


VersionresourceApi apiInstance = new VersionresourceApi();
String id = "id_example"; // String | id
try {
    apiInstance.deleteVersionUsingDELETE(id);
} catch (ApiException e) {
    System.err.println("Exception when calling VersionresourceApi#deleteVersionUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getAllVersionsUsingGET"></a>
# **getAllVersionsUsingGET**
> List&lt;Version&gt; getAllVersionsUsingGET()

getAllVersions

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.VersionresourceApi;


VersionresourceApi apiInstance = new VersionresourceApi();
try {
    List<Version> result = apiInstance.getAllVersionsUsingGET();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VersionresourceApi#getAllVersionsUsingGET");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;Version&gt;**](Version.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getConceptsForVersionUsingGET"></a>
# **getConceptsForVersionUsingGET**
> List&lt;TerminologyConceptImpl&gt; getConceptsForVersionUsingGET(id)

getConceptsForVersion

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.VersionresourceApi;


VersionresourceApi apiInstance = new VersionresourceApi();
String id = "id_example"; // String | The version of the terminology product as date string
try {
    List<TerminologyConceptImpl> result = apiInstance.getConceptsForVersionUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VersionresourceApi#getConceptsForVersionUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| The version of the terminology product as date string |

### Return type

[**List&lt;TerminologyConceptImpl&gt;**](TerminologyConceptImpl.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getDefaultVersionUsingGET"></a>
# **getDefaultVersionUsingGET**
> Version getDefaultVersionUsingGET()

getDefaultVersion

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.VersionresourceApi;


VersionresourceApi apiInstance = new VersionresourceApi();
try {
    Version result = apiInstance.getDefaultVersionUsingGET();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VersionresourceApi#getDefaultVersionUsingGET");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Version**](Version.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getVersionUsingGET"></a>
# **getVersionUsingGET**
> Version getVersionUsingGET(id)

getVersion

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.VersionresourceApi;


VersionresourceApi apiInstance = new VersionresourceApi();
String id = "id_example"; // String | id
try {
    Version result = apiInstance.getVersionUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VersionresourceApi#getVersionUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |

### Return type

[**Version**](Version.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="setDefaultVersionUsingPUT"></a>
# **setDefaultVersionUsingPUT**
> Version setDefaultVersionUsingPUT(id)

setDefaultVersion

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.VersionresourceApi;


VersionresourceApi apiInstance = new VersionresourceApi();
String id = "id_example"; // String | id
try {
    Version result = apiInstance.setDefaultVersionUsingPUT(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VersionresourceApi#setDefaultVersionUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |

### Return type

[**Version**](Version.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateVersionUsingPUT"></a>
# **updateVersionUsingPUT**
> Version updateVersionUsingPUT(version)

updateVersion

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.VersionresourceApi;


VersionresourceApi apiInstance = new VersionresourceApi();
Version version = new Version(); // Version | version
try {
    Version result = apiInstance.updateVersionUsingPUT(version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VersionresourceApi#updateVersionUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **version** | [**Version**](Version.md)| version |

### Return type

[**Version**](Version.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

