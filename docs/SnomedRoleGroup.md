
# SnomedRoleGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relationshipGroupId** | **String** |  |  [optional]
**relationships** | [**CollectionOfSnomedRelationship**](CollectionOfSnomedRelationship.md) |  |  [optional]



