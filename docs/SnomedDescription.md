
# SnomedDescription

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | **Boolean** |  |  [optional]
**caseSignificanceType** | [**CaseSignificanceTypeEnum**](#CaseSignificanceTypeEnum) |  |  [optional]
**conceptId** | **String** |  |  [optional]
**effectiveTime** | [**Calendar**](Calendar.md) |  |  [optional]
**id** | **String** |  |  [optional]
**initialCapStatus** | **Boolean** |  |  [optional]
**language** | **String** |  |  [optional]
**moduleId** | **Long** |  |  [optional]
**source** | **String** |  |  [optional]
**status** | [**StatusEnum**](#StatusEnum) |  |  [optional]
**term** | **String** |  |  [optional]
**type** | [**TypeEnum**](#TypeEnum) |  |  [optional]
**uuid** | **String** |  |  [optional]


<a name="CaseSignificanceTypeEnum"></a>
## Enum: CaseSignificanceTypeEnum
Name | Value
---- | -----
ENTIRE_TERM_CASE_SENSITIVE | &quot;ENTIRE_TERM_CASE_SENSITIVE&quot;
ONLY_INITIAL_CHARACTER_CASE_INSENSITIVE | &quot;ONLY_INITIAL_CHARACTER_CASE_INSENSITIVE&quot;
ENTIRE_TERM_CASE_INSENSITIVE | &quot;ENTIRE_TERM_CASE_INSENSITIVE&quot;
UNKNOWN | &quot;UNKNOWN&quot;


<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
CURRENT | &quot;CURRENT&quot;
RETIRED | &quot;RETIRED&quot;
DUPLICATE | &quot;DUPLICATE&quot;
OUTDATED | &quot;OUTDATED&quot;
AMBIGUOUS | &quot;AMBIGUOUS&quot;
ERRONEOUS | &quot;ERRONEOUS&quot;
LIMITED | &quot;LIMITED&quot;
INAPPROPRIATE | &quot;INAPPROPRIATE&quot;
CONCEPT_NON_CURRENT | &quot;CONCEPT_NON_CURRENT&quot;
MOVED_ELSEWHERE | &quot;MOVED_ELSEWHERE&quot;
PENDING_MOVE | &quot;PENDING_MOVE&quot;
UNKNOWN | &quot;UNKNOWN&quot;


<a name="TypeEnum"></a>
## Enum: TypeEnum
Name | Value
---- | -----
FULLY_SPECIFIED_NAME | &quot;FULLY_SPECIFIED_NAME&quot;
PREFERRED_TERM | &quot;PREFERRED_TERM&quot;
SYNONYM | &quot;SYNONYM&quot;
DEFINITION | &quot;DEFINITION&quot;
UNKNOWN | &quot;UNKNOWN&quot;



