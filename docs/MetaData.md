
# MetaData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acceptedDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**accrualMethods** | [**List&lt;AccrualMethodsEnum&gt;**](#List&lt;AccrualMethodsEnum&gt;) |  |  [optional]
**accrualPeriodicities** | [**List&lt;AccrualPeriodicitiesEnum&gt;**](#List&lt;AccrualPeriodicitiesEnum&gt;) |  |  [optional]
**accrualPolicies** | [**List&lt;AccrualPoliciesEnum&gt;**](#List&lt;AccrualPoliciesEnum&gt;) |  |  [optional]
**audienceEducationLevels** | **List&lt;String&gt;** |  |  [optional]
**audienceMediators** | **List&lt;String&gt;** |  |  [optional]
**availableDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**contributors** | [**List&lt;Person&gt;**](Person.md) |  |  [optional]
**copyrightedDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**createdDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**creators** | [**List&lt;Person&gt;**](Person.md) |  |  [optional]
**description** | **String** |  |  [optional]
**format** | **String** |  |  [optional]
**id** | **String** |  |  [optional]
**identifier** | [**URI**](URI.md) |  |  [optional]
**instructionalMethods** | **List&lt;String&gt;** |  |  [optional]
**issuedDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**language** | **String** |  |  [optional]
**lastUpdatedDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**licenses** | [**List&lt;URI&gt;**](URI.md) |  |  [optional]
**modifiedDates** | [**List&lt;OffsetDateTime&gt;**](OffsetDateTime.md) |  |  [optional]
**provenances** | **List&lt;String&gt;** |  |  [optional]
**publishers** | [**List&lt;Person&gt;**](Person.md) |  |  [optional]
**replacedDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**retiredDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**rightsHolders** | [**List&lt;Person&gt;**](Person.md) |  |  [optional]
**sources** | [**List&lt;URI&gt;**](URI.md) |  |  [optional]
**subject** | **String** |  |  [optional]
**submittedDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**temporalCoverage** | [**CollectionOfCalendar**](CollectionOfCalendar.md) |  |  [optional]
**title** | **String** |  |  [optional]
**type** | [**TypeEnum**](#TypeEnum) |  |  [optional]
**validDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]


<a name="List<AccrualMethodsEnum>"></a>
## Enum: List&lt;AccrualMethodsEnum&gt;
Name | Value
---- | -----
CONSULTATION | &quot;CONSULTATION&quot;
MANDATED | &quot;MANDATED&quot;
REQUESTED | &quot;REQUESTED&quot;


<a name="List<AccrualPeriodicitiesEnum>"></a>
## Enum: List&lt;AccrualPeriodicitiesEnum&gt;
Name | Value
---- | -----
MONTHLY | &quot;MONTHLY&quot;
ANNUAL | &quot;ANNUAL&quot;
BI_ANNUAL | &quot;BI_ANNUAL&quot;
WEEKLY | &quot;WEEKLY&quot;
FORTNIGHTLY | &quot;FORTNIGHTLY&quot;
IRREGULAR | &quot;IRREGULAR&quot;


<a name="List<AccrualPoliciesEnum>"></a>
## Enum: List&lt;AccrualPoliciesEnum&gt;
Name | Value
---- | -----
OPEN | &quot;OPEN&quot;
CLOSED | &quot;CLOSED&quot;


<a name="TypeEnum"></a>
## Enum: TypeEnum
Name | Value
---- | -----
COLLECTION | &quot;COLLECTION&quot;
DATASET | &quot;DATASET&quot;
EVENT | &quot;EVENT&quot;
IMAGE | &quot;IMAGE&quot;
INTERACTIVE_RESOURCE | &quot;INTERACTIVE_RESOURCE&quot;
MOVING_IMAGE | &quot;MOVING_IMAGE&quot;
PHYSICAL_OBJECT | &quot;PHYSICAL_OBJECT&quot;
SERVICE | &quot;SERVICE&quot;
SOFTWARE | &quot;SOFTWARE&quot;
SOUND | &quot;SOUND&quot;
STILL_IMAGE | &quot;STILL_IMAGE&quot;
TEXT | &quot;TEXT&quot;



