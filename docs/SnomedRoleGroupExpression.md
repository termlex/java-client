
# SnomedRoleGroupExpression

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**roleGroup** | [**SnomedRoleGroup**](SnomedRoleGroup.md) |  |  [optional]



