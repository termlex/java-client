
# Expression

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**childExpressions** | [**CollectionOfExpression**](CollectionOfExpression.md) |  |  [optional]
**equivalentExpressions** | [**CollectionOfExpression**](CollectionOfExpression.md) |  |  [optional]
**id** | **String** |  |  [optional]
**parentExpressions** | [**CollectionOfExpression**](CollectionOfExpression.md) |  |  [optional]
**uuid** | **String** |  |  [optional]



