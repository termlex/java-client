# AccountresourceApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**activateAccountUsingGET**](AccountresourceApi.md#activateAccountUsingGET) | **GET** /api/activate | activateAccount
[**authorizeUsingPOST**](AccountresourceApi.md#authorizeUsingPOST) | **POST** /api/authenticate | authorize
[**changePasswordUsingPOST**](AccountresourceApi.md#changePasswordUsingPOST) | **POST** /api/account/change_password | changePassword
[**finishPasswordResetUsingPOST**](AccountresourceApi.md#finishPasswordResetUsingPOST) | **POST** /api/account/reset_password/finish | finishPasswordReset
[**getAccountUsingGET**](AccountresourceApi.md#getAccountUsingGET) | **GET** /api/account | getAccount
[**isAuthenticatedUsingGET**](AccountresourceApi.md#isAuthenticatedUsingGET) | **GET** /api/authenticate | isAuthenticated
[**registerAccountUsingPOST**](AccountresourceApi.md#registerAccountUsingPOST) | **POST** /api/register | registerAccount
[**requestPasswordResetUsingPOST**](AccountresourceApi.md#requestPasswordResetUsingPOST) | **POST** /api/account/reset_password/init | requestPasswordReset
[**saveAccountUsingPOST**](AccountresourceApi.md#saveAccountUsingPOST) | **POST** /api/account | saveAccount


<a name="activateAccountUsingGET"></a>
# **activateAccountUsingGET**
> String activateAccountUsingGET(key)

activateAccount

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.AccountresourceApi;


AccountresourceApi apiInstance = new AccountresourceApi();
String key = "key_example"; // String | key
try {
    String result = apiInstance.activateAccountUsingGET(key);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountresourceApi#activateAccountUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| key |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="authorizeUsingPOST"></a>
# **authorizeUsingPOST**
> Object authorizeUsingPOST(loginDTO)

authorize

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.AccountresourceApi;


AccountresourceApi apiInstance = new AccountresourceApi();
LoginDTO loginDTO = new LoginDTO(); // LoginDTO | loginDTO
try {
    Object result = apiInstance.authorizeUsingPOST(loginDTO);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountresourceApi#authorizeUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loginDTO** | [**LoginDTO**](LoginDTO.md)| loginDTO |

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="changePasswordUsingPOST"></a>
# **changePasswordUsingPOST**
> Object changePasswordUsingPOST(password)

changePassword

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.AccountresourceApi;


AccountresourceApi apiInstance = new AccountresourceApi();
String password = "password_example"; // String | password
try {
    Object result = apiInstance.changePasswordUsingPOST(password);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountresourceApi#changePasswordUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **password** | **String**| password |

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain

<a name="finishPasswordResetUsingPOST"></a>
# **finishPasswordResetUsingPOST**
> String finishPasswordResetUsingPOST(keyAndPassword)

finishPasswordReset

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.AccountresourceApi;


AccountresourceApi apiInstance = new AccountresourceApi();
KeyAndPasswordDTO keyAndPassword = new KeyAndPasswordDTO(); // KeyAndPasswordDTO | keyAndPassword
try {
    String result = apiInstance.finishPasswordResetUsingPOST(keyAndPassword);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountresourceApi#finishPasswordResetUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyAndPassword** | [**KeyAndPasswordDTO**](KeyAndPasswordDTO.md)| keyAndPassword |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain

<a name="getAccountUsingGET"></a>
# **getAccountUsingGET**
> UserDTO getAccountUsingGET()

getAccount

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.AccountresourceApi;


AccountresourceApi apiInstance = new AccountresourceApi();
try {
    UserDTO result = apiInstance.getAccountUsingGET();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountresourceApi#getAccountUsingGET");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**UserDTO**](UserDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="isAuthenticatedUsingGET"></a>
# **isAuthenticatedUsingGET**
> String isAuthenticatedUsingGET()

isAuthenticated

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.AccountresourceApi;


AccountresourceApi apiInstance = new AccountresourceApi();
try {
    String result = apiInstance.isAuthenticatedUsingGET();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountresourceApi#isAuthenticatedUsingGET");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="registerAccountUsingPOST"></a>
# **registerAccountUsingPOST**
> Object registerAccountUsingPOST(userDTO)

registerAccount

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.AccountresourceApi;


AccountresourceApi apiInstance = new AccountresourceApi();
UserDTO userDTO = new UserDTO(); // UserDTO | userDTO
try {
    Object result = apiInstance.registerAccountUsingPOST(userDTO);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountresourceApi#registerAccountUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userDTO** | [**UserDTO**](UserDTO.md)| userDTO |

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain

<a name="requestPasswordResetUsingPOST"></a>
# **requestPasswordResetUsingPOST**
> Object requestPasswordResetUsingPOST(mail)

requestPasswordReset

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.AccountresourceApi;


AccountresourceApi apiInstance = new AccountresourceApi();
String mail = "mail_example"; // String | mail
try {
    Object result = apiInstance.requestPasswordResetUsingPOST(mail);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountresourceApi#requestPasswordResetUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mail** | **String**| mail |

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain

<a name="saveAccountUsingPOST"></a>
# **saveAccountUsingPOST**
> String saveAccountUsingPOST(userDTO)

saveAccount

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.AccountresourceApi;


AccountresourceApi apiInstance = new AccountresourceApi();
UserDTO userDTO = new UserDTO(); // UserDTO | userDTO
try {
    String result = apiInstance.saveAccountUsingPOST(userDTO);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountresourceApi#saveAccountUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userDTO** | [**UserDTO**](UserDTO.md)| userDTO |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

