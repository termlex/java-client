
# SnomedRelationshipPropertyExpression

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relationship** | [**SnomedRelationship**](SnomedRelationship.md) |  |  [optional]



