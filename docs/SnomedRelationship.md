
# SnomedRelationship

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | **Boolean** |  |  [optional]
**characteristicType** | [**CharacteristicTypeEnum**](#CharacteristicTypeEnum) |  |  [optional]
**codeSystem** | [**CodeSystem**](CodeSystem.md) |  |  [optional]
**createdTime** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**definingRelation** | **Boolean** |  |  [optional]
**effectiveTime** | [**Calendar**](Calendar.md) |  |  [optional]
**id** | **String** |  |  [optional]
**inactivatedTime** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**lastUpdatedTime** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**mandatory** | **Boolean** |  |  [optional]
**moduleId** | **Long** |  |  [optional]
**name** | **String** |  |  [optional]
**optional** | **Boolean** |  |  [optional]
**qualifyingRelation** | **Boolean** |  |  [optional]
**refinability** | [**RefinabilityEnum**](#RefinabilityEnum) |  |  [optional]
**refinable** | **Boolean** |  |  [optional]
**relationshipGroup** | **String** |  |  [optional]
**relationshipType** | **String** |  |  [optional]
**source** | **String** |  |  [optional]
**sourceId** | **String** |  |  [optional]
**status** | [**StatusEnum**](#StatusEnum) |  |  [optional]
**targetId** | **String** |  |  [optional]
**uuid** | **String** |  |  [optional]


<a name="CharacteristicTypeEnum"></a>
## Enum: CharacteristicTypeEnum
Name | Value
---- | -----
DEFINING | &quot;DEFINING&quot;
QUALIFIER | &quot;QUALIFIER&quot;
HISTORICAL | &quot;HISTORICAL&quot;
ADDITIONAL | &quot;ADDITIONAL&quot;
STATED | &quot;STATED&quot;
INFERRED | &quot;INFERRED&quot;
UNKNOWN | &quot;UNKNOWN&quot;


<a name="RefinabilityEnum"></a>
## Enum: RefinabilityEnum
Name | Value
---- | -----
MANDATORY | &quot;MANDATORY&quot;
OPTIONAL | &quot;OPTIONAL&quot;
NOT_REFINABLE | &quot;NOT_REFINABLE&quot;
UNKNOWN | &quot;UNKNOWN&quot;


<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
CURRENT | &quot;CURRENT&quot;
RETIRED | &quot;RETIRED&quot;
DUPLICATE | &quot;DUPLICATE&quot;
OUTDATED | &quot;OUTDATED&quot;
AMBIGUOUS | &quot;AMBIGUOUS&quot;
ERRONEOUS | &quot;ERRONEOUS&quot;
LIMITED | &quot;LIMITED&quot;
INAPPROPRIATE | &quot;INAPPROPRIATE&quot;
CONCEPT_NON_CURRENT | &quot;CONCEPT_NON_CURRENT&quot;
MOVED_ELSEWHERE | &quot;MOVED_ELSEWHERE&quot;
PENDING_MOVE | &quot;PENDING_MOVE&quot;
UNKNOWN | &quot;UNKNOWN&quot;



