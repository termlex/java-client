# QueryresourceApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addConstraintInQueryUsingPOST**](QueryresourceApi.md#addConstraintInQueryUsingPOST) | **POST** /api/queries/{id}/constraints/included | Sets the included constraint for a query component expression.
[**addContainedExpressionsForQueryUsingPUT**](QueryresourceApi.md#addContainedExpressionsForQueryUsingPUT) | **PUT** /api/queries/{id}/children | adds child expressions to query with a given uuid
[**addExcludedConstraintsInQueryUsingPUT**](QueryresourceApi.md#addExcludedConstraintsInQueryUsingPUT) | **PUT** /api/queries/{id}/constraints/excluded | Adds the excluded constraints to a query component expression.
[**createQueryUsingPOST**](QueryresourceApi.md#createQueryUsingPOST) | **POST** /api/queries/{type} | creates a query based on the type requested (defaults to QueryComponent).
[**createQueryUsingPOST1**](QueryresourceApi.md#createQueryUsingPOST1) | **POST** /api/queries | creates a query based on the type requested (defaults to QueryComponent).
[**deleteConstraintInQueryComponentUsingDELETE**](QueryresourceApi.md#deleteConstraintInQueryComponentUsingDELETE) | **DELETE** /api/queries/{id}/constraints/included | Deletes the included constraint for a query component expression.
[**deleteQueryUsingDELETE**](QueryresourceApi.md#deleteQueryUsingDELETE) | **DELETE** /api/queries/{id} | deleteQuery
[**getAllQueriesUsingGET**](QueryresourceApi.md#getAllQueriesUsingGET) | **GET** /api/queries | getAllQueries
[**getConstraintInQueryUsingGET**](QueryresourceApi.md#getConstraintInQueryUsingGET) | **GET** /api/queries/{id}/constraints/included | Gets the included constraint for a query component expression.
[**getConstraintsInQueryUsingGET**](QueryresourceApi.md#getConstraintsInQueryUsingGET) | **GET** /api/queries/{id}/constraints | Gets the included and excluded constraints for a query component expression.
[**getContainedExpressionsForQueryUsingGET**](QueryresourceApi.md#getContainedExpressionsForQueryUsingGET) | **GET** /api/queries/{id}/children | returns contained child expressions for query with a given uuid
[**getExcludedConstraintsInQueryUsingGET**](QueryresourceApi.md#getExcludedConstraintsInQueryUsingGET) | **GET** /api/queries/{id}/constraints/excluded | Gets the excluded constraints for a query component expression.
[**getQueryUsingGET**](QueryresourceApi.md#getQueryUsingGET) | **GET** /api/queries/{id} | getQuery
[**getSubsumedConceptsCountForQueryUsingGET**](QueryresourceApi.md#getSubsumedConceptsCountForQueryUsingGET) | **GET** /api/queries/{id}/conceptcount | returns subsumed concepts count for query for a given uuid
[**getSubsumedConceptsForQueryUsingGET**](QueryresourceApi.md#getSubsumedConceptsForQueryUsingGET) | **GET** /api/queries/{id}/concepts | returns subsumed concepts for query for a given uuid
[**removeContainedExpressionsForQueryUsingDELETE**](QueryresourceApi.md#removeContainedExpressionsForQueryUsingDELETE) | **DELETE** /api/queries/{id}/children | removes child expressions from query with a given uuid
[**removeExcludedConstraintsInQueryUsingDELETE**](QueryresourceApi.md#removeExcludedConstraintsInQueryUsingDELETE) | **DELETE** /api/queries/{id}/constraints/excluded | Removes the excluded constraints from a query component expression.
[**setContainedExpressionsForQueryUsingPOST**](QueryresourceApi.md#setContainedExpressionsForQueryUsingPOST) | **POST** /api/queries/{id}/children | sets contained child expressions for query with a given uuid
[**setExcludedConstraintsInQueryUsingPOST**](QueryresourceApi.md#setExcludedConstraintsInQueryUsingPOST) | **POST** /api/queries/{id}/constraints/excluded | Sets the excluded constraints for a query component expression.
[**updateConstraintInQueryComponentUsingPUT**](QueryresourceApi.md#updateConstraintInQueryComponentUsingPUT) | **PUT** /api/queries/{id}/constraints/included | Updates the included constraint for a query component expression.
[**updateQueryUsingPUT**](QueryresourceApi.md#updateQueryUsingPUT) | **PUT** /api/queries/{id} | updates query with id


<a name="addConstraintInQueryUsingPOST"></a>
# **addConstraintInQueryUsingPOST**
> QueryExpression addConstraintInQueryUsingPOST(id, constraint)

Sets the included constraint for a query component expression.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.QueryresourceApi;


QueryresourceApi apiInstance = new QueryresourceApi();
String id = "id_example"; // String | id
Object constraint = null; // Object | constraint
try {
    QueryExpression result = apiInstance.addConstraintInQueryUsingPOST(id, constraint);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling QueryresourceApi#addConstraintInQueryUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **constraint** | **Object**| constraint |

### Return type

[**QueryExpression**](QueryExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="addContainedExpressionsForQueryUsingPUT"></a>
# **addContainedExpressionsForQueryUsingPUT**
> QueryExpression addContainedExpressionsForQueryUsingPUT(id, containedExpressionsToAdd)

adds child expressions to query with a given uuid

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.QueryresourceApi;


QueryresourceApi apiInstance = new QueryresourceApi();
String id = "id_example"; // String | id
List<Object> containedExpressionsToAdd = Arrays.asList(new List<Object>()); // List<Object> | containedExpressionsToAdd
try {
    QueryExpression result = apiInstance.addContainedExpressionsForQueryUsingPUT(id, containedExpressionsToAdd);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling QueryresourceApi#addContainedExpressionsForQueryUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **containedExpressionsToAdd** | **List&lt;Object&gt;**| containedExpressionsToAdd |

### Return type

[**QueryExpression**](QueryExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="addExcludedConstraintsInQueryUsingPUT"></a>
# **addExcludedConstraintsInQueryUsingPUT**
> QueryExpression addExcludedConstraintsInQueryUsingPUT(id, constraintsToAdd)

Adds the excluded constraints to a query component expression.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.QueryresourceApi;


QueryresourceApi apiInstance = new QueryresourceApi();
String id = "id_example"; // String | id
List<Object> constraintsToAdd = Arrays.asList(new List<Object>()); // List<Object> | constraintsToAdd
try {
    QueryExpression result = apiInstance.addExcludedConstraintsInQueryUsingPUT(id, constraintsToAdd);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling QueryresourceApi#addExcludedConstraintsInQueryUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **constraintsToAdd** | **List&lt;Object&gt;**| constraintsToAdd |

### Return type

[**QueryExpression**](QueryExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="createQueryUsingPOST"></a>
# **createQueryUsingPOST**
> QueryExpression createQueryUsingPOST(type, containedEntities)

creates a query based on the type requested (defaults to QueryComponent).

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.QueryresourceApi;


QueryresourceApi apiInstance = new QueryresourceApi();
String type = "type_example"; // String | type
List<Object> containedEntities = Arrays.asList(new List<Object>()); // List<Object> | containedEntities
try {
    QueryExpression result = apiInstance.createQueryUsingPOST(type, containedEntities);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling QueryresourceApi#createQueryUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **String**| type |
 **containedEntities** | **List&lt;Object&gt;**| containedEntities |

### Return type

[**QueryExpression**](QueryExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="createQueryUsingPOST1"></a>
# **createQueryUsingPOST1**
> QueryExpression createQueryUsingPOST1(type, content)

creates a query based on the type requested (defaults to QueryComponent).

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.QueryresourceApi;


QueryresourceApi apiInstance = new QueryresourceApi();
String type = "type_example"; // String | type
Object content = null; // Object | content
try {
    QueryExpression result = apiInstance.createQueryUsingPOST1(type, content);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling QueryresourceApi#createQueryUsingPOST1");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **String**| type | [optional]
 **content** | **Object**| content | [optional]

### Return type

[**QueryExpression**](QueryExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="deleteConstraintInQueryComponentUsingDELETE"></a>
# **deleteConstraintInQueryComponentUsingDELETE**
> QueryExpression deleteConstraintInQueryComponentUsingDELETE(id)

Deletes the included constraint for a query component expression.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.QueryresourceApi;


QueryresourceApi apiInstance = new QueryresourceApi();
String id = "id_example"; // String | id
try {
    QueryExpression result = apiInstance.deleteConstraintInQueryComponentUsingDELETE(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling QueryresourceApi#deleteConstraintInQueryComponentUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |

### Return type

[**QueryExpression**](QueryExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="deleteQueryUsingDELETE"></a>
# **deleteQueryUsingDELETE**
> deleteQueryUsingDELETE(id)

deleteQuery

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.QueryresourceApi;


QueryresourceApi apiInstance = new QueryresourceApi();
String id = "id_example"; // String | id
try {
    apiInstance.deleteQueryUsingDELETE(id);
} catch (ApiException e) {
    System.err.println("Exception when calling QueryresourceApi#deleteQueryUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getAllQueriesUsingGET"></a>
# **getAllQueriesUsingGET**
> List&lt;QueryExpression&gt; getAllQueriesUsingGET()

getAllQueries

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.QueryresourceApi;


QueryresourceApi apiInstance = new QueryresourceApi();
try {
    List<QueryExpression> result = apiInstance.getAllQueriesUsingGET();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling QueryresourceApi#getAllQueriesUsingGET");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;QueryExpression&gt;**](QueryExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getConstraintInQueryUsingGET"></a>
# **getConstraintInQueryUsingGET**
> TerminologyConstraint getConstraintInQueryUsingGET(id)

Gets the included constraint for a query component expression.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.QueryresourceApi;


QueryresourceApi apiInstance = new QueryresourceApi();
String id = "id_example"; // String | id
try {
    TerminologyConstraint result = apiInstance.getConstraintInQueryUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling QueryresourceApi#getConstraintInQueryUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |

### Return type

[**TerminologyConstraint**](TerminologyConstraint.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getConstraintsInQueryUsingGET"></a>
# **getConstraintsInQueryUsingGET**
> Object getConstraintsInQueryUsingGET(id)

Gets the included and excluded constraints for a query component expression.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.QueryresourceApi;


QueryresourceApi apiInstance = new QueryresourceApi();
String id = "id_example"; // String | id
try {
    Object result = apiInstance.getConstraintsInQueryUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling QueryresourceApi#getConstraintsInQueryUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getContainedExpressionsForQueryUsingGET"></a>
# **getContainedExpressionsForQueryUsingGET**
> List&lt;Object&gt; getContainedExpressionsForQueryUsingGET(id)

returns contained child expressions for query with a given uuid

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.QueryresourceApi;


QueryresourceApi apiInstance = new QueryresourceApi();
String id = "id_example"; // String | id
try {
    List<Object> result = apiInstance.getContainedExpressionsForQueryUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling QueryresourceApi#getContainedExpressionsForQueryUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |

### Return type

**List&lt;Object&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getExcludedConstraintsInQueryUsingGET"></a>
# **getExcludedConstraintsInQueryUsingGET**
> List&lt;Object&gt; getExcludedConstraintsInQueryUsingGET(id)

Gets the excluded constraints for a query component expression.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.QueryresourceApi;


QueryresourceApi apiInstance = new QueryresourceApi();
String id = "id_example"; // String | id
try {
    List<Object> result = apiInstance.getExcludedConstraintsInQueryUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling QueryresourceApi#getExcludedConstraintsInQueryUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |

### Return type

**List&lt;Object&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getQueryUsingGET"></a>
# **getQueryUsingGET**
> QueryExpression getQueryUsingGET(id)

getQuery

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.QueryresourceApi;


QueryresourceApi apiInstance = new QueryresourceApi();
String id = "id_example"; // String | id
try {
    QueryExpression result = apiInstance.getQueryUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling QueryresourceApi#getQueryUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |

### Return type

[**QueryExpression**](QueryExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getSubsumedConceptsCountForQueryUsingGET"></a>
# **getSubsumedConceptsCountForQueryUsingGET**
> List&lt;Object&gt; getSubsumedConceptsCountForQueryUsingGET(id)

returns subsumed concepts count for query for a given uuid

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.QueryresourceApi;


QueryresourceApi apiInstance = new QueryresourceApi();
String id = "id_example"; // String | id
try {
    List<Object> result = apiInstance.getSubsumedConceptsCountForQueryUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling QueryresourceApi#getSubsumedConceptsCountForQueryUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |

### Return type

**List&lt;Object&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getSubsumedConceptsForQueryUsingGET"></a>
# **getSubsumedConceptsForQueryUsingGET**
> List&lt;Object&gt; getSubsumedConceptsForQueryUsingGET(id)

returns subsumed concepts for query for a given uuid

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.QueryresourceApi;


QueryresourceApi apiInstance = new QueryresourceApi();
String id = "id_example"; // String | id
try {
    List<Object> result = apiInstance.getSubsumedConceptsForQueryUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling QueryresourceApi#getSubsumedConceptsForQueryUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |

### Return type

**List&lt;Object&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="removeContainedExpressionsForQueryUsingDELETE"></a>
# **removeContainedExpressionsForQueryUsingDELETE**
> QueryExpression removeContainedExpressionsForQueryUsingDELETE(id, containedExpressions)

removes child expressions from query with a given uuid

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.QueryresourceApi;


QueryresourceApi apiInstance = new QueryresourceApi();
String id = "id_example"; // String | id
List<Object> containedExpressions = Arrays.asList(new List<Object>()); // List<Object> | containedExpressions
try {
    QueryExpression result = apiInstance.removeContainedExpressionsForQueryUsingDELETE(id, containedExpressions);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling QueryresourceApi#removeContainedExpressionsForQueryUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **containedExpressions** | **List&lt;Object&gt;**| containedExpressions |

### Return type

[**QueryExpression**](QueryExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="removeExcludedConstraintsInQueryUsingDELETE"></a>
# **removeExcludedConstraintsInQueryUsingDELETE**
> QueryExpression removeExcludedConstraintsInQueryUsingDELETE(id, excludedConstraints)

Removes the excluded constraints from a query component expression.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.QueryresourceApi;


QueryresourceApi apiInstance = new QueryresourceApi();
String id = "id_example"; // String | id
List<Object> excludedConstraints = Arrays.asList(new List<Object>()); // List<Object> | excludedConstraints
try {
    QueryExpression result = apiInstance.removeExcludedConstraintsInQueryUsingDELETE(id, excludedConstraints);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling QueryresourceApi#removeExcludedConstraintsInQueryUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **excludedConstraints** | **List&lt;Object&gt;**| excludedConstraints |

### Return type

[**QueryExpression**](QueryExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="setContainedExpressionsForQueryUsingPOST"></a>
# **setContainedExpressionsForQueryUsingPOST**
> QueryExpression setContainedExpressionsForQueryUsingPOST(id, containedExpressions)

sets contained child expressions for query with a given uuid

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.QueryresourceApi;


QueryresourceApi apiInstance = new QueryresourceApi();
String id = "id_example"; // String | id
List<Object> containedExpressions = Arrays.asList(new List<Object>()); // List<Object> | containedExpressions
try {
    QueryExpression result = apiInstance.setContainedExpressionsForQueryUsingPOST(id, containedExpressions);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling QueryresourceApi#setContainedExpressionsForQueryUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **containedExpressions** | **List&lt;Object&gt;**| containedExpressions |

### Return type

[**QueryExpression**](QueryExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="setExcludedConstraintsInQueryUsingPOST"></a>
# **setExcludedConstraintsInQueryUsingPOST**
> QueryExpression setExcludedConstraintsInQueryUsingPOST(id, excludedConstraints)

Sets the excluded constraints for a query component expression.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.QueryresourceApi;


QueryresourceApi apiInstance = new QueryresourceApi();
String id = "id_example"; // String | id
List<Map> excludedConstraints = Arrays.asList(new Map()); // List<Map> | excludedConstraints
try {
    QueryExpression result = apiInstance.setExcludedConstraintsInQueryUsingPOST(id, excludedConstraints);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling QueryresourceApi#setExcludedConstraintsInQueryUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **excludedConstraints** | [**List&lt;Map&gt;**](Map.md)| excludedConstraints |

### Return type

[**QueryExpression**](QueryExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="updateConstraintInQueryComponentUsingPUT"></a>
# **updateConstraintInQueryComponentUsingPUT**
> QueryExpression updateConstraintInQueryComponentUsingPUT(id, includedId, subsumptionFlavour)

Updates the included constraint for a query component expression.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.QueryresourceApi;


QueryresourceApi apiInstance = new QueryresourceApi();
String id = "id_example"; // String | id
String includedId = "includedId_example"; // String | includedId
String subsumptionFlavour = "subsumptionFlavour_example"; // String | subsumptionFlavour
try {
    QueryExpression result = apiInstance.updateConstraintInQueryComponentUsingPUT(id, includedId, subsumptionFlavour);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling QueryresourceApi#updateConstraintInQueryComponentUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **includedId** | **String**| includedId |
 **subsumptionFlavour** | **String**| subsumptionFlavour | [optional] [enum: SELF_OR_ANY_TYPE_OF, SELF_ONLY, ANY_TYPE_OF_BUT_NOT_SELF, NOT_SELF, NOT_SELF_OR_ANY_TYPE_OF, UNKNOWN]

### Return type

[**QueryExpression**](QueryExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="updateQueryUsingPUT"></a>
# **updateQueryUsingPUT**
> QueryExpression updateQueryUsingPUT(id, content)

updates query with id

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.QueryresourceApi;


QueryresourceApi apiInstance = new QueryresourceApi();
String id = "id_example"; // String | id
Object content = null; // Object | content
try {
    QueryExpression result = apiInstance.updateQueryUsingPUT(id, content);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling QueryresourceApi#updateQueryUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **content** | **Object**| content |

### Return type

[**QueryExpression**](QueryExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

