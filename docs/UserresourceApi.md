# UserresourceApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createUserUsingPOST**](UserresourceApi.md#createUserUsingPOST) | **POST** /api/users | createUser
[**deleteUserUsingDELETE**](UserresourceApi.md#deleteUserUsingDELETE) | **DELETE** /api/users/{login} | deleteUser
[**getAllUsersUsingGET**](UserresourceApi.md#getAllUsersUsingGET) | **GET** /api/users | getAllUsers
[**getUserUsingGET**](UserresourceApi.md#getUserUsingGET) | **GET** /api/users/{login} | getUser
[**updateUserUsingPUT**](UserresourceApi.md#updateUserUsingPUT) | **PUT** /api/users | updateUser


<a name="createUserUsingPOST"></a>
# **createUserUsingPOST**
> Object createUserUsingPOST(managedUserDTO)

createUser

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.UserresourceApi;


UserresourceApi apiInstance = new UserresourceApi();
ManagedUserDTO managedUserDTO = new ManagedUserDTO(); // ManagedUserDTO | managedUserDTO
try {
    Object result = apiInstance.createUserUsingPOST(managedUserDTO);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserresourceApi#createUserUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **managedUserDTO** | [**ManagedUserDTO**](ManagedUserDTO.md)| managedUserDTO |

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteUserUsingDELETE"></a>
# **deleteUserUsingDELETE**
> deleteUserUsingDELETE(login)

deleteUser

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.UserresourceApi;


UserresourceApi apiInstance = new UserresourceApi();
String login = "login_example"; // String | login
try {
    apiInstance.deleteUserUsingDELETE(login);
} catch (ApiException e) {
    System.err.println("Exception when calling UserresourceApi#deleteUserUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **login** | **String**| login |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getAllUsersUsingGET"></a>
# **getAllUsersUsingGET**
> List&lt;ManagedUserDTO&gt; getAllUsersUsingGET()

getAllUsers

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.UserresourceApi;


UserresourceApi apiInstance = new UserresourceApi();
try {
    List<ManagedUserDTO> result = apiInstance.getAllUsersUsingGET();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserresourceApi#getAllUsersUsingGET");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;ManagedUserDTO&gt;**](ManagedUserDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getUserUsingGET"></a>
# **getUserUsingGET**
> ManagedUserDTO getUserUsingGET(login)

getUser

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.UserresourceApi;


UserresourceApi apiInstance = new UserresourceApi();
String login = "login_example"; // String | login
try {
    ManagedUserDTO result = apiInstance.getUserUsingGET(login);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserresourceApi#getUserUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **login** | **String**| login |

### Return type

[**ManagedUserDTO**](ManagedUserDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateUserUsingPUT"></a>
# **updateUserUsingPUT**
> ManagedUserDTO updateUserUsingPUT(managedUserDTO)

updateUser

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.UserresourceApi;


UserresourceApi apiInstance = new UserresourceApi();
ManagedUserDTO managedUserDTO = new ManagedUserDTO(); // ManagedUserDTO | managedUserDTO
try {
    ManagedUserDTO result = apiInstance.updateUserUsingPUT(managedUserDTO);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserresourceApi#updateUserUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **managedUserDTO** | [**ManagedUserDTO**](ManagedUserDTO.md)| managedUserDTO |

### Return type

[**ManagedUserDTO**](ManagedUserDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

