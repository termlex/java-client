
# ManagedUserDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**activated** | **Boolean** |  |  [optional]
**authorities** | **List&lt;String&gt;** |  |  [optional]
**createdDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**email** | **String** |  |  [optional]
**firstName** | **String** |  |  [optional]
**id** | **String** |  |  [optional]
**langKey** | **String** |  |  [optional]
**lastModifiedBy** | **String** |  |  [optional]
**lastModifiedDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**lastName** | **String** |  |  [optional]
**login** | **String** |  |  [optional]
**password** | **String** |  |  [optional]



