
# Person

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**age** | **Long** |  |  [optional]
**ageYears** | **Integer** |  |  [optional]
**dob** | [**Calendar**](Calendar.md) |  |  [optional]
**email** | **String** |  |  [optional]
**firstName** | **String** |  |  [optional]
**gender** | [**GenderEnum**](#GenderEnum) |  |  [optional]
**id** | **Long** |  |  [optional]
**lastName** | **String** |  |  [optional]
**middleName** | **String** |  |  [optional]


<a name="GenderEnum"></a>
## Enum: GenderEnum
Name | Value
---- | -----
MALE | &quot;MALE&quot;
FEMALE | &quot;FEMALE&quot;
UNKNOWN | &quot;UNKNOWN&quot;



