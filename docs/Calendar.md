
# Calendar

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**calendarType** | **String** |  |  [optional]
**fieldsComputed** | **Integer** |  |  [optional]
**fieldsNormalized** | **Integer** |  |  [optional]
**firstDayOfWeek** | **Integer** |  |  [optional]
**lenient** | **Boolean** |  |  [optional]
**minimalDaysInFirstWeek** | **Integer** |  |  [optional]
**time** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**timeInMillis** | **Long** |  |  [optional]
**timeZone** | [**TimeZone**](TimeZone.md) |  |  [optional]
**weekCountData** | [**Locale**](Locale.md) |  |  [optional]
**weekDateSupported** | **Boolean** |  |  [optional]
**weekYear** | **Integer** |  |  [optional]
**weeksInWeekYear** | **Integer** |  |  [optional]
**zoneShared** | **Boolean** |  |  [optional]



