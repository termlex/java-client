# RefsetresourceApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addDefinitionsUsingPUT**](RefsetresourceApi.md#addDefinitionsUsingPUT) | **PUT** /api/refsets/{id}/definitions | adds definitions to refset with given id. This does not clear existing definitions.
[**addMembersUsingPUT**](RefsetresourceApi.md#addMembersUsingPUT) | **PUT** /api/refsets/{id}/members | adds collection as members of refset with given id. This does not clear existing members.
[**createDefinitionsUsingPOST**](RefsetresourceApi.md#createDefinitionsUsingPOST) | **POST** /api/refsets/{id}/definitions | sets collection as definitions of refset with given id. Note this clears existing definitions.
[**createMembersUsingPOST**](RefsetresourceApi.md#createMembersUsingPOST) | **POST** /api/refsets/{id}/members | set collection as members of refset with given id. Note this clears existing members.
[**createRefsetUsingPOST**](RefsetresourceApi.md#createRefsetUsingPOST) | **POST** /api/refsets | createRefset
[**deleteRefsetUsingDELETE**](RefsetresourceApi.md#deleteRefsetUsingDELETE) | **DELETE** /api/refsets/{id} | deleteRefset
[**getAllRefsetsUsingGET**](RefsetresourceApi.md#getAllRefsetsUsingGET) | **GET** /api/refsets | getAllRefsets
[**getRefsetBySctIdUsingGET**](RefsetresourceApi.md#getRefsetBySctIdUsingGET) | **GET** /api/refsets/sct/{id} | returns refset for a given SNOMED CT id
[**getRefsetByUuidUsingGET**](RefsetresourceApi.md#getRefsetByUuidUsingGET) | **GET** /api/refsets/uuid/{id} | returns refset for a given uuid
[**getRefsetDefinitionsUsingGET**](RefsetresourceApi.md#getRefsetDefinitionsUsingGET) | **GET** /api/refsets/{id}/definitions | returns definitions of refset with given id
[**getRefsetMemberCountUsingGET**](RefsetresourceApi.md#getRefsetMemberCountUsingGET) | **GET** /api/refsets/{id}/members/count | returns member count for refset with given id
[**getRefsetMembersUsingGET**](RefsetresourceApi.md#getRefsetMembersUsingGET) | **GET** /api/refsets/{id}/members | returns members of refset with given id
[**getRefsetUsingGET**](RefsetresourceApi.md#getRefsetUsingGET) | **GET** /api/refsets/{id} | getRefset
[**getRefsetsInModuleUsingGET**](RefsetresourceApi.md#getRefsetsInModuleUsingGET) | **GET** /api/refsets/module/{id} | gets all refsets that belong to module with a given id
[**removeDefinitionsUsingDELETE**](RefsetresourceApi.md#removeDefinitionsUsingDELETE) | **DELETE** /api/refsets/{id}/definitions | deletes definitions from refset with given id
[**removeMembersUsingDELETE**](RefsetresourceApi.md#removeMembersUsingDELETE) | **DELETE** /api/refsets/{id}/members | removes collection from members of refset with given id
[**searchRefsetUsingGET**](RefsetresourceApi.md#searchRefsetUsingGET) | **GET** /api/refsets/{id}/version/{version}/search | Returns matches in a refset for given token
[**updateRefsetUsingPUT**](RefsetresourceApi.md#updateRefsetUsingPUT) | **PUT** /api/refsets | updateRefset


<a name="addDefinitionsUsingPUT"></a>
# **addDefinitionsUsingPUT**
> Refset addDefinitionsUsingPUT(id, definitions)

adds definitions to refset with given id. This does not clear existing definitions.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.RefsetresourceApi;


RefsetresourceApi apiInstance = new RefsetresourceApi();
String id = "id_example"; // String | id
List<Object> definitions = Arrays.asList(new List<Object>()); // List<Object> | definitions
try {
    Refset result = apiInstance.addDefinitionsUsingPUT(id, definitions);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RefsetresourceApi#addDefinitionsUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **definitions** | **List&lt;Object&gt;**| definitions |

### Return type

[**Refset**](Refset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="addMembersUsingPUT"></a>
# **addMembersUsingPUT**
> Refset addMembersUsingPUT(id, members)

adds collection as members of refset with given id. This does not clear existing members.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.RefsetresourceApi;


RefsetresourceApi apiInstance = new RefsetresourceApi();
String id = "id_example"; // String | id
List<String> members = Arrays.asList(new List<String>()); // List<String> | members
try {
    Refset result = apiInstance.addMembersUsingPUT(id, members);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RefsetresourceApi#addMembersUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **members** | **List&lt;String&gt;**| members |

### Return type

[**Refset**](Refset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="createDefinitionsUsingPOST"></a>
# **createDefinitionsUsingPOST**
> Refset createDefinitionsUsingPOST(id, definitions)

sets collection as definitions of refset with given id. Note this clears existing definitions.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.RefsetresourceApi;


RefsetresourceApi apiInstance = new RefsetresourceApi();
String id = "id_example"; // String | id
List<Object> definitions = Arrays.asList(new List<Object>()); // List<Object> | definitions
try {
    Refset result = apiInstance.createDefinitionsUsingPOST(id, definitions);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RefsetresourceApi#createDefinitionsUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **definitions** | **List&lt;Object&gt;**| definitions |

### Return type

[**Refset**](Refset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="createMembersUsingPOST"></a>
# **createMembersUsingPOST**
> Refset createMembersUsingPOST(id, members)

set collection as members of refset with given id. Note this clears existing members.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.RefsetresourceApi;


RefsetresourceApi apiInstance = new RefsetresourceApi();
String id = "id_example"; // String | id
List<String> members = Arrays.asList(new List<String>()); // List<String> | members
try {
    Refset result = apiInstance.createMembersUsingPOST(id, members);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RefsetresourceApi#createMembersUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **members** | **List&lt;String&gt;**| members |

### Return type

[**Refset**](Refset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="createRefsetUsingPOST"></a>
# **createRefsetUsingPOST**
> Refset createRefsetUsingPOST(refset)

createRefset

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.RefsetresourceApi;


RefsetresourceApi apiInstance = new RefsetresourceApi();
Refset refset = new Refset(); // Refset | refset
try {
    Refset result = apiInstance.createRefsetUsingPOST(refset);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RefsetresourceApi#createRefsetUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **refset** | [**Refset**](Refset.md)| refset |

### Return type

[**Refset**](Refset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteRefsetUsingDELETE"></a>
# **deleteRefsetUsingDELETE**
> deleteRefsetUsingDELETE(id)

deleteRefset

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.RefsetresourceApi;


RefsetresourceApi apiInstance = new RefsetresourceApi();
String id = "id_example"; // String | id
try {
    apiInstance.deleteRefsetUsingDELETE(id);
} catch (ApiException e) {
    System.err.println("Exception when calling RefsetresourceApi#deleteRefsetUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getAllRefsetsUsingGET"></a>
# **getAllRefsetsUsingGET**
> List&lt;Refset&gt; getAllRefsetsUsingGET()

getAllRefsets

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.RefsetresourceApi;


RefsetresourceApi apiInstance = new RefsetresourceApi();
try {
    List<Refset> result = apiInstance.getAllRefsetsUsingGET();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RefsetresourceApi#getAllRefsetsUsingGET");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;Refset&gt;**](Refset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getRefsetBySctIdUsingGET"></a>
# **getRefsetBySctIdUsingGET**
> Refset getRefsetBySctIdUsingGET(id)

returns refset for a given SNOMED CT id

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.RefsetresourceApi;


RefsetresourceApi apiInstance = new RefsetresourceApi();
Long id = 789L; // Long | id
try {
    Refset result = apiInstance.getRefsetBySctIdUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RefsetresourceApi#getRefsetBySctIdUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |

### Return type

[**Refset**](Refset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getRefsetByUuidUsingGET"></a>
# **getRefsetByUuidUsingGET**
> Refset getRefsetByUuidUsingGET(id)

returns refset for a given uuid

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.RefsetresourceApi;


RefsetresourceApi apiInstance = new RefsetresourceApi();
String id = "id_example"; // String | id
try {
    Refset result = apiInstance.getRefsetByUuidUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RefsetresourceApi#getRefsetByUuidUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |

### Return type

[**Refset**](Refset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getRefsetDefinitionsUsingGET"></a>
# **getRefsetDefinitionsUsingGET**
> List&lt;Object&gt; getRefsetDefinitionsUsingGET(id)

returns definitions of refset with given id

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.RefsetresourceApi;


RefsetresourceApi apiInstance = new RefsetresourceApi();
String id = "id_example"; // String | id
try {
    List<Object> result = apiInstance.getRefsetDefinitionsUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RefsetresourceApi#getRefsetDefinitionsUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |

### Return type

**List&lt;Object&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getRefsetMemberCountUsingGET"></a>
# **getRefsetMemberCountUsingGET**
> Long getRefsetMemberCountUsingGET(id)

returns member count for refset with given id

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.RefsetresourceApi;


RefsetresourceApi apiInstance = new RefsetresourceApi();
String id = "id_example"; // String | id
try {
    Long result = apiInstance.getRefsetMemberCountUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RefsetresourceApi#getRefsetMemberCountUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |

### Return type

**Long**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getRefsetMembersUsingGET"></a>
# **getRefsetMembersUsingGET**
> Refset getRefsetMembersUsingGET(id)

returns members of refset with given id

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.RefsetresourceApi;


RefsetresourceApi apiInstance = new RefsetresourceApi();
String id = "id_example"; // String | id
try {
    Refset result = apiInstance.getRefsetMembersUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RefsetresourceApi#getRefsetMembersUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |

### Return type

[**Refset**](Refset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getRefsetUsingGET"></a>
# **getRefsetUsingGET**
> Refset getRefsetUsingGET(id)

getRefset

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.RefsetresourceApi;


RefsetresourceApi apiInstance = new RefsetresourceApi();
String id = "id_example"; // String | id
try {
    Refset result = apiInstance.getRefsetUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RefsetresourceApi#getRefsetUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |

### Return type

[**Refset**](Refset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getRefsetsInModuleUsingGET"></a>
# **getRefsetsInModuleUsingGET**
> List&lt;Object&gt; getRefsetsInModuleUsingGET(id)

gets all refsets that belong to module with a given id

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.RefsetresourceApi;


RefsetresourceApi apiInstance = new RefsetresourceApi();
Long id = 789L; // Long | id
try {
    List<Object> result = apiInstance.getRefsetsInModuleUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RefsetresourceApi#getRefsetsInModuleUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |

### Return type

**List&lt;Object&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="removeDefinitionsUsingDELETE"></a>
# **removeDefinitionsUsingDELETE**
> Refset removeDefinitionsUsingDELETE(id, definitions)

deletes definitions from refset with given id

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.RefsetresourceApi;


RefsetresourceApi apiInstance = new RefsetresourceApi();
String id = "id_example"; // String | id
List<Object> definitions = Arrays.asList(new List<Object>()); // List<Object> | definitions
try {
    Refset result = apiInstance.removeDefinitionsUsingDELETE(id, definitions);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RefsetresourceApi#removeDefinitionsUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **definitions** | **List&lt;Object&gt;**| definitions |

### Return type

[**Refset**](Refset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="removeMembersUsingDELETE"></a>
# **removeMembersUsingDELETE**
> Refset removeMembersUsingDELETE(id, members)

removes collection from members of refset with given id

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.RefsetresourceApi;


RefsetresourceApi apiInstance = new RefsetresourceApi();
String id = "id_example"; // String | id
List<String> members = Arrays.asList(new List<String>()); // List<String> | members
try {
    Refset result = apiInstance.removeMembersUsingDELETE(id, members);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RefsetresourceApi#removeMembersUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **members** | **List&lt;String&gt;**| members |

### Return type

[**Refset**](Refset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="searchRefsetUsingGET"></a>
# **searchRefsetUsingGET**
> Object searchRefsetUsingGET(term, id, version, languageCode)

Returns matches in a refset for given token

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.RefsetresourceApi;


RefsetresourceApi apiInstance = new RefsetresourceApi();
String term = "term_example"; // String | Token to search for; can be a term or an id
String id = "id_example"; // String | The id of the refset
String version = "version_example"; // String | The version of the terminology product as date string. Defaults to default version specified in server instance
String languageCode = "en"; // String | The language of descriptions to search
try {
    Object result = apiInstance.searchRefsetUsingGET(term, id, version, languageCode);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RefsetresourceApi#searchRefsetUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **term** | **String**| Token to search for; can be a term or an id |
 **id** | **String**| The id of the refset |
 **version** | **String**| The version of the terminology product as date string. Defaults to default version specified in server instance |
 **languageCode** | **String**| The language of descriptions to search | [optional] [default to en]

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateRefsetUsingPUT"></a>
# **updateRefsetUsingPUT**
> Refset updateRefsetUsingPUT(refset)

updateRefset

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.RefsetresourceApi;


RefsetresourceApi apiInstance = new RefsetresourceApi();
Refset refset = new Refset(); // Refset | refset
try {
    Refset result = apiInstance.updateRefsetUsingPUT(refset);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RefsetresourceApi#updateRefsetUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **refset** | [**Refset**](Refset.md)| refset |

### Return type

[**Refset**](Refset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

