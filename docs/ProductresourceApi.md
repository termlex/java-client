# ProductresourceApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createProductUsingPOST**](ProductresourceApi.md#createProductUsingPOST) | **POST** /api/products | createProduct
[**deleteProductUsingDELETE**](ProductresourceApi.md#deleteProductUsingDELETE) | **DELETE** /api/products/{id} | deleteProduct
[**getAllProductsUsingGET**](ProductresourceApi.md#getAllProductsUsingGET) | **GET** /api/products | getAllProducts
[**getProductUsingGET**](ProductresourceApi.md#getProductUsingGET) | **GET** /api/products/{id} | getProduct
[**getProductVersionsUsingGET**](ProductresourceApi.md#getProductVersionsUsingGET) | **GET** /api/products/{id}/versions | getProductVersions
[**updateProductUsingPUT**](ProductresourceApi.md#updateProductUsingPUT) | **PUT** /api/products | updateProduct


<a name="createProductUsingPOST"></a>
# **createProductUsingPOST**
> CodeSystem createProductUsingPOST(product)

createProduct

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ProductresourceApi;


ProductresourceApi apiInstance = new ProductresourceApi();
CodeSystem product = new CodeSystem(); // CodeSystem | product
try {
    CodeSystem result = apiInstance.createProductUsingPOST(product);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductresourceApi#createProductUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product** | [**CodeSystem**](CodeSystem.md)| product |

### Return type

[**CodeSystem**](CodeSystem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteProductUsingDELETE"></a>
# **deleteProductUsingDELETE**
> deleteProductUsingDELETE(id)

deleteProduct

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ProductresourceApi;


ProductresourceApi apiInstance = new ProductresourceApi();
String id = "id_example"; // String | id
try {
    apiInstance.deleteProductUsingDELETE(id);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductresourceApi#deleteProductUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getAllProductsUsingGET"></a>
# **getAllProductsUsingGET**
> List&lt;CodeSystem&gt; getAllProductsUsingGET()

getAllProducts

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ProductresourceApi;


ProductresourceApi apiInstance = new ProductresourceApi();
try {
    List<CodeSystem> result = apiInstance.getAllProductsUsingGET();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductresourceApi#getAllProductsUsingGET");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;CodeSystem&gt;**](CodeSystem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getProductUsingGET"></a>
# **getProductUsingGET**
> CodeSystem getProductUsingGET(id)

getProduct

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ProductresourceApi;


ProductresourceApi apiInstance = new ProductresourceApi();
String id = "id_example"; // String | id
try {
    CodeSystem result = apiInstance.getProductUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductresourceApi#getProductUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |

### Return type

[**CodeSystem**](CodeSystem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getProductVersionsUsingGET"></a>
# **getProductVersionsUsingGET**
> List&lt;Version&gt; getProductVersionsUsingGET(id)

getProductVersions

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ProductresourceApi;


ProductresourceApi apiInstance = new ProductresourceApi();
String id = "id_example"; // String | id
try {
    List<Version> result = apiInstance.getProductVersionsUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductresourceApi#getProductVersionsUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |

### Return type

[**List&lt;Version&gt;**](Version.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateProductUsingPUT"></a>
# **updateProductUsingPUT**
> CodeSystem updateProductUsingPUT(object)

updateProduct

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ProductresourceApi;


ProductresourceApi apiInstance = new ProductresourceApi();
Object object = null; // Object | object
try {
    CodeSystem result = apiInstance.updateProductUsingPUT(object);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductresourceApi#updateProductUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **object** | **Object**| object |

### Return type

[**CodeSystem**](CodeSystem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

