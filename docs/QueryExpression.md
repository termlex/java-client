
# QueryExpression

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**containedExpressions** | [**List&lt;QueryExpression&gt;**](QueryExpression.md) |  |  [optional]
**humanReadableStatement** | **String** |  |  [optional]
**id** | **String** |  |  [optional]
**metaData** | [**MetaData**](MetaData.md) |  |  [optional]
**runTimeStatus** | [**RunTimeStatusEnum**](#RunTimeStatusEnum) |  |  [optional]
**type** | **String** |  |  [optional]
**uuid** | **String** |  |  [optional]


<a name="RunTimeStatusEnum"></a>
## Enum: RunTimeStatusEnum
Name | Value
---- | -----
SKIP | &quot;SKIP&quot;
EXECUTE | &quot;EXECUTE&quot;



