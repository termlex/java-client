
# Refset

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**copyright** | **String** |  |  [optional]
**createdTime** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**date** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**definitions** | [**List&lt;QueryExpression&gt;**](QueryExpression.md) |  |  [optional]
**description** | **String** |  |  [optional]
**experimental** | **Boolean** |  |  [optional]
**extensible** | **Boolean** |  |  [optional]
**id** | **String** |  |  [optional]
**members** | **List&lt;String&gt;** |  |  [optional]
**moduleId** | **Long** |  |  [optional]
**name** | **String** |  |  [optional]
**ownerId** | **String** |  |  [optional]
**publisher** | [**Organisation**](Organisation.md) |  |  [optional]
**sctId** | **Long** |  |  [optional]
**shortName** | **String** |  |  [optional]
**status** | [**StatusEnum**](#StatusEnum) |  |  [optional]
**type** | [**TypeEnum**](#TypeEnum) |  |  [optional]
**updatedBy** | **String** |  |  [optional]


<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
DRAFT | &quot;DRAFT&quot;
ACTIVE | &quot;ACTIVE&quot;
RETIRED | &quot;RETIRED&quot;


<a name="TypeEnum"></a>
## Enum: TypeEnum
Name | Value
---- | -----
SCT | &quot;SCT&quot;
SCT_RF2 | &quot;SCT_RF2&quot;
SCT_RF1 | &quot;SCT_RF1&quot;
ICD | &quot;ICD&quot;
LOINC | &quot;LOINC&quot;
DMD | &quot;DMD&quot;
ACT | &quot;ACT&quot;
NICIP | &quot;NICIP&quot;
READ | &quot;READ&quot;
CTV | &quot;CTV&quot;
REFSET | &quot;REFSET&quot;
ICD_O | &quot;ICD_O&quot;
VALUESET | &quot;VALUESET&quot;



