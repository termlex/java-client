
# TerminologyConcept

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | **Boolean** |  |  [optional]
**activeConcept** | **Boolean** |  |  [optional]
**codeSystem** | [**CodeSystem**](CodeSystem.md) |  |  [optional]
**createdTime** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**fullySpecifiedName** | **String** |  |  [optional]
**id** | **String** |  |  [optional]
**inactivatedTime** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**lastUpdatedTime** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**preferredTerm** | **String** |  |  [optional]
**source** | **String** |  |  [optional]
**synonyms** | [**CollectionOfstring**](CollectionOfstring.md) |  |  [optional]
**type** | [**TypeEnum**](#TypeEnum) |  |  [optional]


<a name="TypeEnum"></a>
## Enum: TypeEnum
Name | Value
---- | -----
FINDING_WEC | &quot;FINDING_WEC&quot;
PROCEDURE_WEC | &quot;PROCEDURE_WEC&quot;
FINDING | &quot;FINDING&quot;
PROCEDURE | &quot;PROCEDURE&quot;
OBSERVABLE_ENTITY | &quot;OBSERVABLE_ENTITY&quot;
SUBSTANCE | &quot;SUBSTANCE&quot;
LINKAGE_CONCEPT | &quot;LINKAGE_CONCEPT&quot;
STAGING_AND_SCALES | &quot;STAGING_AND_SCALES&quot;
PHYSICAL_OBJECT | &quot;PHYSICAL_OBJECT&quot;
ENVIRONMENT_OR_GEOGRAPHICAL_LOCATION | &quot;ENVIRONMENT_OR_GEOGRAPHICAL_LOCATION&quot;
SPECIAL_CONCEPT | &quot;SPECIAL_CONCEPT&quot;
RECORD_ARTEFACT | &quot;RECORD_ARTEFACT&quot;
PHYSICAL_FORCE | &quot;PHYSICAL_FORCE&quot;
SOCIAL_CONTEXT | &quot;SOCIAL_CONTEXT&quot;
QUALIFIER_VALUE | &quot;QUALIFIER_VALUE&quot;
ORGANISM | &quot;ORGANISM&quot;
SPECIMEN | &quot;SPECIMEN&quot;
BODY_STRUCTURE | &quot;BODY_STRUCTURE&quot;
DISEASE | &quot;DISEASE&quot;
EVALUATION_PROCEDURE | &quot;EVALUATION_PROCEDURE&quot;
EVENT | &quot;EVENT&quot;
SURGICAL_PROCEDURE | &quot;SURGICAL_PROCEDURE&quot;
PHARMACEUTICAL_OR_BIOLOGICAL_PRODUCT | &quot;PHARMACEUTICAL_OR_BIOLOGICAL_PRODUCT&quot;
SITUATION_WEC | &quot;SITUATION_WEC&quot;
DRUG_DEVICE_COMBO_PRODUCT | &quot;DRUG_DEVICE_COMBO_PRODUCT&quot;
DEFINITELY_NOT_PRESENT | &quot;DEFINITELY_NOT_PRESENT&quot;
KNOWN_ABSENT | &quot;KNOWN_ABSENT&quot;
ATTRIBUTE_FINDING_CONTEXT | &quot;ATTRIBUTE_FINDING_CONTEXT&quot;
ATTRIBUTE_TEMPORAL_CONTEXT | &quot;ATTRIBUTE_TEMPORAL_CONTEXT&quot;
ATTRIBUTE_PROCEDURE_CONTEXT | &quot;ATTRIBUTE_PROCEDURE_CONTEXT&quot;
ATTRIBUTE_SUBJECT_RELATIONSHIP_CONTEXT | &quot;ATTRIBUTE_SUBJECT_RELATIONSHIP_CONTEXT&quot;
ATTRIBUTE_ASSOCIATED_FINDING | &quot;ATTRIBUTE_ASSOCIATED_FINDING&quot;
ATTRIBUTE_ASSOCIATED_PROCEDURE | &quot;ATTRIBUTE_ASSOCIATED_PROCEDURE&quot;
ATTRIBUTE_EPISODICITY | &quot;ATTRIBUTE_EPISODICITY&quot;
ATTRIBUTE_COURSE | &quot;ATTRIBUTE_COURSE&quot;
ATTRIBUTE_SEVERITIES | &quot;ATTRIBUTE_SEVERITIES&quot;
ATTRIBUTE_ASSOCIATED_MORPHOLOGY | &quot;ATTRIBUTE_ASSOCIATED_MORPHOLOGY&quot;
ATTRIBUTE_FINDING_SITE | &quot;ATTRIBUTE_FINDING_SITE&quot;
ATTRIBUTE_PRIORITY | &quot;ATTRIBUTE_PRIORITY&quot;
ATTRIBUTE_METHOD | &quot;ATTRIBUTE_METHOD&quot;
ATTRIBUTE_DIRECT_PROCEDURE_SITE | &quot;ATTRIBUTE_DIRECT_PROCEDURE_SITE&quot;
ATTRIBUTE_INDIRECT_PROCEDURE_SITE | &quot;ATTRIBUTE_INDIRECT_PROCEDURE_SITE&quot;
ATTRIBUTE_PROCEDURE_SITE | &quot;ATTRIBUTE_PROCEDURE_SITE&quot;
ATTRIBUTE_USING_DEVICE | &quot;ATTRIBUTE_USING_DEVICE&quot;
ATTRIBUTE_USING_ACCESS_DEVICE | &quot;ATTRIBUTE_USING_ACCESS_DEVICE&quot;
ATTRIBUTE_USING_ENERGY | &quot;ATTRIBUTE_USING_ENERGY&quot;
ATTRIBUTE_USING_SUBSTANCE | &quot;ATTRIBUTE_USING_SUBSTANCE&quot;
ATTRIBUTE_VIA | &quot;ATTRIBUTE_VIA&quot;
ATTRIBUTE_ACCESS | &quot;ATTRIBUTE_ACCESS&quot;
ATTRIBUTE_SURGICAL_APPROACH | &quot;ATTRIBUTE_SURGICAL_APPROACH&quot;
ATTRIBUTE_LATERALITY | &quot;ATTRIBUTE_LATERALITY&quot;
ATTRIBUTE_IS_A | &quot;ATTRIBUTE_IS_A&quot;
ATTRIBUTE_DIRECT_SUBSTANCE | &quot;ATTRIBUTE_DIRECT_SUBSTANCE&quot;
SCT_MODEL_COMPONENT | &quot;SCT_MODEL_COMPONENT&quot;
SCT_TOP_CONCEPT | &quot;SCT_TOP_CONCEPT&quot;
UNKNOWN | &quot;UNKNOWN&quot;



