# ConceptresourceApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createConceptUsingPOST**](ConceptresourceApi.md#createConceptUsingPOST) | **POST** /api/concepts | createConcept
[**deleteConceptUsingDELETE**](ConceptresourceApi.md#deleteConceptUsingDELETE) | **DELETE** /api/concepts/{id} | deleteConcept
[**getAllConceptsUsingGET**](ConceptresourceApi.md#getAllConceptsUsingGET) | **GET** /api/concepts | getAllConcepts
[**getComponentStatusForIdUsingGET**](ConceptresourceApi.md#getComponentStatusForIdUsingGET) | **GET** /api/concepts/version/{version}/id/{id}/status | get the status of concept with given id
[**getCompositionalGrammarFormForIdUsingGET**](ConceptresourceApi.md#getCompositionalGrammarFormForIdUsingGET) | **GET** /api/concepts/version/{version}/id/{id}/cgf | get the compositional grammar form of concept with given id
[**getConceptForIdUsingGET**](ConceptresourceApi.md#getConceptForIdUsingGET) | **GET** /api/concepts/version/{version}/id/{id} | get the concept with given id
[**getConceptFullySpecifiedNameForIdUsingGET**](ConceptresourceApi.md#getConceptFullySpecifiedNameForIdUsingGET) | **GET** /api/concepts/version/{version}/id/{id}/fsn | get the Fully Specified Name of concept with given id and language
[**getConceptPreferredTermForIdUsingGET**](ConceptresourceApi.md#getConceptPreferredTermForIdUsingGET) | **GET** /api/concepts/version/{version}/id/{id}/pt | get the Preferred Term of concept with given id
[**getConceptTypeForIdUsingGET**](ConceptresourceApi.md#getConceptTypeForIdUsingGET) | **GET** /api/concepts/version/{version}/id/{id}/type | get the type of concept with given id
[**getConceptUsingGET**](ConceptresourceApi.md#getConceptUsingGET) | **GET** /api/concepts/{id} | getConcept
[**getDeltaContentUsingGET**](ConceptresourceApi.md#getDeltaContentUsingGET) | **GET** /api/concepts/version/{version}/delta/all | get all delta content for the release
[**getLongNormalFormForIdUsingGET**](ConceptresourceApi.md#getLongNormalFormForIdUsingGET) | **GET** /api/concepts/version/{version}/id/{id}/lnf | get the long normal form of concept with given id
[**getNewlyAddedContentUsingGET**](ConceptresourceApi.md#getNewlyAddedContentUsingGET) | **GET** /api/concepts/version/{version}/delta/new | get newly added content for the release
[**getNewlyModifiedContentUsingGET**](ConceptresourceApi.md#getNewlyModifiedContentUsingGET) | **GET** /api/concepts/version/{version}/delta/modified | get still active concepts modified in the release
[**getNewlyResurrectedContentUsingGET**](ConceptresourceApi.md#getNewlyResurrectedContentUsingGET) | **GET** /api/concepts/version/{version}/delta/resurrected | get newly resurrected content for the release
[**getNewlyRetiredContentUsingGET**](ConceptresourceApi.md#getNewlyRetiredContentUsingGET) | **GET** /api/concepts/version/{version}/delta/retired | get newly retired content for the release
[**getProspectiveReplacementsForIdUsingGET**](ConceptresourceApi.md#getProspectiveReplacementsForIdUsingGET) | **GET** /api/concepts/version/{version}/id/{id}/history/prospective | get the prospective replacement map for concept with given id
[**getRetrospectiveReplacementsForIdUsingGET**](ConceptresourceApi.md#getRetrospectiveReplacementsForIdUsingGET) | **GET** /api/concepts/version/{version}/id/{id}/history/retroprospective | get the retrospective replacement map concept with given id
[**getShortNormalFormForIdUsingGET**](ConceptresourceApi.md#getShortNormalFormForIdUsingGET) | **GET** /api/concepts/version/{version}/id/{id}/snf | get the short normal form of concept with given id
[**getSubsumptionRelationshipUsingGET**](ConceptresourceApi.md#getSubsumptionRelationshipUsingGET) | **GET** /api/concepts/compare | get the subsumption relationship of comparing two concepts
[**updateConceptUsingPUT**](ConceptresourceApi.md#updateConceptUsingPUT) | **PUT** /api/concepts | updateConcept


<a name="createConceptUsingPOST"></a>
# **createConceptUsingPOST**
> TerminologyConceptImpl createConceptUsingPOST(concept)

createConcept

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConceptresourceApi;


ConceptresourceApi apiInstance = new ConceptresourceApi();
TerminologyConceptImpl concept = new TerminologyConceptImpl(); // TerminologyConceptImpl | concept
try {
    TerminologyConceptImpl result = apiInstance.createConceptUsingPOST(concept);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConceptresourceApi#createConceptUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **concept** | [**TerminologyConceptImpl**](TerminologyConceptImpl.md)| concept |

### Return type

[**TerminologyConceptImpl**](TerminologyConceptImpl.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteConceptUsingDELETE"></a>
# **deleteConceptUsingDELETE**
> deleteConceptUsingDELETE(id)

deleteConcept

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConceptresourceApi;


ConceptresourceApi apiInstance = new ConceptresourceApi();
String id = "id_example"; // String | id
try {
    apiInstance.deleteConceptUsingDELETE(id);
} catch (ApiException e) {
    System.err.println("Exception when calling ConceptresourceApi#deleteConceptUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getAllConceptsUsingGET"></a>
# **getAllConceptsUsingGET**
> List&lt;TerminologyConceptImpl&gt; getAllConceptsUsingGET()

getAllConcepts

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConceptresourceApi;


ConceptresourceApi apiInstance = new ConceptresourceApi();
try {
    List<TerminologyConceptImpl> result = apiInstance.getAllConceptsUsingGET();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConceptresourceApi#getAllConceptsUsingGET");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;TerminologyConceptImpl&gt;**](TerminologyConceptImpl.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getComponentStatusForIdUsingGET"></a>
# **getComponentStatusForIdUsingGET**
> String getComponentStatusForIdUsingGET(id, version)

get the status of concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConceptresourceApi;


ConceptresourceApi apiInstance = new ConceptresourceApi();
Long id = 789L; // Long | id
String version = "version_example"; // String | The version of the terminology product as date string
try {
    String result = apiInstance.getComponentStatusForIdUsingGET(id, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConceptresourceApi#getComponentStatusForIdUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **version** | **String**| The version of the terminology product as date string |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getCompositionalGrammarFormForIdUsingGET"></a>
# **getCompositionalGrammarFormForIdUsingGET**
> String getCompositionalGrammarFormForIdUsingGET(id, version)

get the compositional grammar form of concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConceptresourceApi;


ConceptresourceApi apiInstance = new ConceptresourceApi();
Long id = 789L; // Long | id
String version = "version_example"; // String | The version of the terminology product as date string
try {
    String result = apiInstance.getCompositionalGrammarFormForIdUsingGET(id, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConceptresourceApi#getCompositionalGrammarFormForIdUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **version** | **String**| The version of the terminology product as date string |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getConceptForIdUsingGET"></a>
# **getConceptForIdUsingGET**
> SnomedConcept getConceptForIdUsingGET(id, version, flavour)

get the concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConceptresourceApi;


ConceptresourceApi apiInstance = new ConceptresourceApi();
Long id = 789L; // Long | id
String version = "version_example"; // String | The version of the terminology product as date string
String flavour = "flavour_example"; // String | The flavour of the concept - specifies details retrieved
try {
    SnomedConcept result = apiInstance.getConceptForIdUsingGET(id, version, flavour);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConceptresourceApi#getConceptForIdUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **version** | **String**| The version of the terminology product as date string |
 **flavour** | **String**| The flavour of the concept - specifies details retrieved | [optional] [enum: ID_ONLY, ID_LABEL, ID_DESCRIPTIONS, ID_DESCRIPTIONS_RELATIONSHIPS, ID_DESCRIPTIONS_STATED_RELATIONSHIPS]

### Return type

[**SnomedConcept**](SnomedConcept.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getConceptFullySpecifiedNameForIdUsingGET"></a>
# **getConceptFullySpecifiedNameForIdUsingGET**
> String getConceptFullySpecifiedNameForIdUsingGET(id, language, version)

get the Fully Specified Name of concept with given id and language

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConceptresourceApi;


ConceptresourceApi apiInstance = new ConceptresourceApi();
String id = "id_example"; // String | id
String language = "language_example"; // String | language
String version = "version_example"; // String | The version of the terminology product as date string
try {
    String result = apiInstance.getConceptFullySpecifiedNameForIdUsingGET(id, language, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConceptresourceApi#getConceptFullySpecifiedNameForIdUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **language** | **String**| language |
 **version** | **String**| The version of the terminology product as date string |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getConceptPreferredTermForIdUsingGET"></a>
# **getConceptPreferredTermForIdUsingGET**
> String getConceptPreferredTermForIdUsingGET(id, language, version)

get the Preferred Term of concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConceptresourceApi;


ConceptresourceApi apiInstance = new ConceptresourceApi();
String id = "id_example"; // String | id
String language = "language_example"; // String | language
String version = "version_example"; // String | The version of the terminology product as date string
try {
    String result = apiInstance.getConceptPreferredTermForIdUsingGET(id, language, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConceptresourceApi#getConceptPreferredTermForIdUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **language** | **String**| language |
 **version** | **String**| The version of the terminology product as date string |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getConceptTypeForIdUsingGET"></a>
# **getConceptTypeForIdUsingGET**
> String getConceptTypeForIdUsingGET(id, version)

get the type of concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConceptresourceApi;


ConceptresourceApi apiInstance = new ConceptresourceApi();
String id = "id_example"; // String | id
String version = "version_example"; // String | The version of the terminology product as date string
try {
    String result = apiInstance.getConceptTypeForIdUsingGET(id, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConceptresourceApi#getConceptTypeForIdUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **version** | **String**| The version of the terminology product as date string |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getConceptUsingGET"></a>
# **getConceptUsingGET**
> TerminologyConceptImpl getConceptUsingGET(id)

getConcept

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConceptresourceApi;


ConceptresourceApi apiInstance = new ConceptresourceApi();
String id = "id_example"; // String | id
try {
    TerminologyConceptImpl result = apiInstance.getConceptUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConceptresourceApi#getConceptUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |

### Return type

[**TerminologyConceptImpl**](TerminologyConceptImpl.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getDeltaContentUsingGET"></a>
# **getDeltaContentUsingGET**
> List&lt;Object&gt; getDeltaContentUsingGET(version)

get all delta content for the release

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConceptresourceApi;


ConceptresourceApi apiInstance = new ConceptresourceApi();
String version = "version_example"; // String | The version of the terminology product as date string
try {
    List<Object> result = apiInstance.getDeltaContentUsingGET(version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConceptresourceApi#getDeltaContentUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **version** | **String**| The version of the terminology product as date string |

### Return type

**List&lt;Object&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getLongNormalFormForIdUsingGET"></a>
# **getLongNormalFormForIdUsingGET**
> NormalFormExpression getLongNormalFormForIdUsingGET(id, version)

get the long normal form of concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConceptresourceApi;


ConceptresourceApi apiInstance = new ConceptresourceApi();
Long id = 789L; // Long | id
String version = "version_example"; // String | The version of the terminology product as date string
try {
    NormalFormExpression result = apiInstance.getLongNormalFormForIdUsingGET(id, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConceptresourceApi#getLongNormalFormForIdUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **version** | **String**| The version of the terminology product as date string |

### Return type

[**NormalFormExpression**](NormalFormExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getNewlyAddedContentUsingGET"></a>
# **getNewlyAddedContentUsingGET**
> List&lt;Object&gt; getNewlyAddedContentUsingGET(version)

get newly added content for the release

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConceptresourceApi;


ConceptresourceApi apiInstance = new ConceptresourceApi();
String version = "version_example"; // String | The version of the terminology product as date string
try {
    List<Object> result = apiInstance.getNewlyAddedContentUsingGET(version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConceptresourceApi#getNewlyAddedContentUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **version** | **String**| The version of the terminology product as date string |

### Return type

**List&lt;Object&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getNewlyModifiedContentUsingGET"></a>
# **getNewlyModifiedContentUsingGET**
> List&lt;Object&gt; getNewlyModifiedContentUsingGET(version)

get still active concepts modified in the release

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConceptresourceApi;


ConceptresourceApi apiInstance = new ConceptresourceApi();
String version = "version_example"; // String | The version of the terminology product as date string
try {
    List<Object> result = apiInstance.getNewlyModifiedContentUsingGET(version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConceptresourceApi#getNewlyModifiedContentUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **version** | **String**| The version of the terminology product as date string |

### Return type

**List&lt;Object&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getNewlyResurrectedContentUsingGET"></a>
# **getNewlyResurrectedContentUsingGET**
> List&lt;Object&gt; getNewlyResurrectedContentUsingGET(version)

get newly resurrected content for the release

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConceptresourceApi;


ConceptresourceApi apiInstance = new ConceptresourceApi();
String version = "version_example"; // String | The version of the terminology product as date string
try {
    List<Object> result = apiInstance.getNewlyResurrectedContentUsingGET(version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConceptresourceApi#getNewlyResurrectedContentUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **version** | **String**| The version of the terminology product as date string |

### Return type

**List&lt;Object&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getNewlyRetiredContentUsingGET"></a>
# **getNewlyRetiredContentUsingGET**
> List&lt;Object&gt; getNewlyRetiredContentUsingGET(version)

get newly retired content for the release

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConceptresourceApi;


ConceptresourceApi apiInstance = new ConceptresourceApi();
String version = "version_example"; // String | The version of the terminology product as date string
try {
    List<Object> result = apiInstance.getNewlyRetiredContentUsingGET(version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConceptresourceApi#getNewlyRetiredContentUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **version** | **String**| The version of the terminology product as date string |

### Return type

**List&lt;Object&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getProspectiveReplacementsForIdUsingGET"></a>
# **getProspectiveReplacementsForIdUsingGET**
> Object getProspectiveReplacementsForIdUsingGET(id, version)

get the prospective replacement map for concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConceptresourceApi;


ConceptresourceApi apiInstance = new ConceptresourceApi();
Long id = 789L; // Long | id
String version = "version_example"; // String | The version of the terminology product as date string
try {
    Object result = apiInstance.getProspectiveReplacementsForIdUsingGET(id, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConceptresourceApi#getProspectiveReplacementsForIdUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **version** | **String**| The version of the terminology product as date string |

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getRetrospectiveReplacementsForIdUsingGET"></a>
# **getRetrospectiveReplacementsForIdUsingGET**
> Object getRetrospectiveReplacementsForIdUsingGET(id, version)

get the retrospective replacement map concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConceptresourceApi;


ConceptresourceApi apiInstance = new ConceptresourceApi();
Long id = 789L; // Long | id
String version = "version_example"; // String | The version of the terminology product as date string
try {
    Object result = apiInstance.getRetrospectiveReplacementsForIdUsingGET(id, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConceptresourceApi#getRetrospectiveReplacementsForIdUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **version** | **String**| The version of the terminology product as date string |

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getShortNormalFormForIdUsingGET"></a>
# **getShortNormalFormForIdUsingGET**
> NormalFormExpression getShortNormalFormForIdUsingGET(id, version)

get the short normal form of concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConceptresourceApi;


ConceptresourceApi apiInstance = new ConceptresourceApi();
Long id = 789L; // Long | id
String version = "version_example"; // String | The version of the terminology product as date string
try {
    NormalFormExpression result = apiInstance.getShortNormalFormForIdUsingGET(id, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConceptresourceApi#getShortNormalFormForIdUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **version** | **String**| The version of the terminology product as date string |

### Return type

[**NormalFormExpression**](NormalFormExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getSubsumptionRelationshipUsingGET"></a>
# **getSubsumptionRelationshipUsingGET**
> String getSubsumptionRelationshipUsingGET(conceptId1, conceptId2, version)

get the subsumption relationship of comparing two concepts

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConceptresourceApi;


ConceptresourceApi apiInstance = new ConceptresourceApi();
String conceptId1 = "conceptId1_example"; // String | conceptId1
String conceptId2 = "conceptId2_example"; // String | conceptId2
String version = "version_example"; // String | The version of the terminology product as date string
try {
    String result = apiInstance.getSubsumptionRelationshipUsingGET(conceptId1, conceptId2, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConceptresourceApi#getSubsumptionRelationshipUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **conceptId1** | **String**| conceptId1 |
 **conceptId2** | **String**| conceptId2 |
 **version** | **String**| The version of the terminology product as date string | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="updateConceptUsingPUT"></a>
# **updateConceptUsingPUT**
> TerminologyConceptImpl updateConceptUsingPUT(concept)

updateConcept

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConceptresourceApi;


ConceptresourceApi apiInstance = new ConceptresourceApi();
TerminologyConceptImpl concept = new TerminologyConceptImpl(); // TerminologyConceptImpl | concept
try {
    TerminologyConceptImpl result = apiInstance.updateConceptUsingPUT(concept);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConceptresourceApi#updateConceptUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **concept** | [**TerminologyConceptImpl**](TerminologyConceptImpl.md)| concept |

### Return type

[**TerminologyConceptImpl**](TerminologyConceptImpl.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

