
# UserDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**activated** | **Boolean** |  |  [optional]
**authorities** | **List&lt;String&gt;** |  |  [optional]
**email** | **String** |  |  [optional]
**firstName** | **String** |  |  [optional]
**langKey** | **String** |  |  [optional]
**lastName** | **String** |  |  [optional]
**login** | **String** |  |  [optional]
**password** | **String** |  |  [optional]



