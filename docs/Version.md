
# Version

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**productId** | **String** |  |  [optional]
**publishedDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]



