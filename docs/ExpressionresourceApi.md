# ExpressionresourceApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getCompositionalGrammarFormUsingGET**](ExpressionresourceApi.md#getCompositionalGrammarFormUsingGET) | **GET** /api/expressions/cgf | get the compositional grammar form of expression
[**getLongNormalFormUsingGET**](ExpressionresourceApi.md#getLongNormalFormUsingGET) | **GET** /api/expressions/lnf | get the long normal form of expression
[**getShortNormalFormUsingGET**](ExpressionresourceApi.md#getShortNormalFormUsingGET) | **GET** /api/expressions/snf | get the long normal form of expression
[**getSubsumptionRelationshipUsingGET1**](ExpressionresourceApi.md#getSubsumptionRelationshipUsingGET1) | **GET** /api/expressions/compare | get the subsumption relationship of comparing two expressions


<a name="getCompositionalGrammarFormUsingGET"></a>
# **getCompositionalGrammarFormUsingGET**
> String getCompositionalGrammarFormUsingGET(expression)

get the compositional grammar form of expression

When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ExpressionresourceApi;


ExpressionresourceApi apiInstance = new ExpressionresourceApi();
String expression = "expression_example"; // String | The expression to be rendered
try {
    String result = apiInstance.getCompositionalGrammarFormUsingGET(expression);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ExpressionresourceApi#getCompositionalGrammarFormUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **expression** | **String**| The expression to be rendered | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getLongNormalFormUsingGET"></a>
# **getLongNormalFormUsingGET**
> NormalFormExpression getLongNormalFormUsingGET(expression, version)

get the long normal form of expression

When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ExpressionresourceApi;


ExpressionresourceApi apiInstance = new ExpressionresourceApi();
String expression = "expression_example"; // String | The expression to be rendered
String version = "version_example"; // String | The version of the terminology product as date string
try {
    NormalFormExpression result = apiInstance.getLongNormalFormUsingGET(expression, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ExpressionresourceApi#getLongNormalFormUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **expression** | **String**| The expression to be rendered | [optional]
 **version** | **String**| The version of the terminology product as date string | [optional]

### Return type

[**NormalFormExpression**](NormalFormExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getShortNormalFormUsingGET"></a>
# **getShortNormalFormUsingGET**
> NormalFormExpression getShortNormalFormUsingGET(expression, version)

get the long normal form of expression

When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ExpressionresourceApi;


ExpressionresourceApi apiInstance = new ExpressionresourceApi();
String expression = "expression_example"; // String | The expression to be rendered
String version = "version_example"; // String | The version of the terminology product as date string
try {
    NormalFormExpression result = apiInstance.getShortNormalFormUsingGET(expression, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ExpressionresourceApi#getShortNormalFormUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **expression** | **String**| The expression to be rendered | [optional]
 **version** | **String**| The version of the terminology product as date string | [optional]

### Return type

[**NormalFormExpression**](NormalFormExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getSubsumptionRelationshipUsingGET1"></a>
# **getSubsumptionRelationshipUsingGET1**
> String getSubsumptionRelationshipUsingGET1(expression1, expression2, version)

get the subsumption relationship of comparing two expressions

When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ExpressionresourceApi;


ExpressionresourceApi apiInstance = new ExpressionresourceApi();
String expression1 = "expression1_example"; // String | The left hand side expression to be compared
String expression2 = "expression2_example"; // String | The right hand side expression to be compared
String version = "version_example"; // String | The version of the terminology product as date string
try {
    String result = apiInstance.getSubsumptionRelationshipUsingGET1(expression1, expression2, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ExpressionresourceApi#getSubsumptionRelationshipUsingGET1");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **expression1** | **String**| The left hand side expression to be compared | [optional]
 **expression2** | **String**| The right hand side expression to be compared | [optional]
 **version** | **String**| The version of the terminology product as date string | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

