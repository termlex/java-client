# ConstraintresourceApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createConstraintUsingPOST**](ConstraintresourceApi.md#createConstraintUsingPOST) | **POST** /api/constraints/{id} | creates a query based on the conceptId and subsumption flavour (defaults to SubsumptionFlavour.SELF_OR_ANY_TYPE_OF).
[**deleteConstraintUsingDELETE**](ConstraintresourceApi.md#deleteConstraintUsingDELETE) | **DELETE** /api/constraints/{id} | deleteConstraint
[**getAllConstraintsUsingGET**](ConstraintresourceApi.md#getAllConstraintsUsingGET) | **GET** /api/constraints | getAllConstraints
[**getConstraintUsingGET**](ConstraintresourceApi.md#getConstraintUsingGET) | **GET** /api/constraints/{id} | getConstraint
[**getSubsumedConceptCountForConstraintUsingGET**](ConstraintresourceApi.md#getSubsumedConceptCountForConstraintUsingGET) | **GET** /api/constraints/{id}/conceptcount | returns subsumed concepts count for constraint with a given uuid
[**getSubsumedConceptsForConstraintUsingGET**](ConstraintresourceApi.md#getSubsumedConceptsForConstraintUsingGET) | **GET** /api/constraints/{id}/concepts | returns subsumed concepts for constraint with a given uuid
[**getSubsumptionForConstraintUsingGET**](ConstraintresourceApi.md#getSubsumptionForConstraintUsingGET) | **GET** /api/constraints/{id}/flavour | gets the subsumption flavour for constraint with id (defaults to SubsumptionFlavour.SELF_OR_ANY_TYPE_OF).
[**setSubsumptionForConstraintUsingPUT**](ConstraintresourceApi.md#setSubsumptionForConstraintUsingPUT) | **PUT** /api/constraints/{id}/flavour | updates the subsumption flavour for constraint with id (defaults to SubsumptionFlavour.SELF_OR_ANY_TYPE_OF).
[**updateConstraintUsingPUT**](ConstraintresourceApi.md#updateConstraintUsingPUT) | **PUT** /api/constraints/{id} | updates constraint with id


<a name="createConstraintUsingPOST"></a>
# **createConstraintUsingPOST**
> TerminologyConstraint createConstraintUsingPOST(id, flavour)

creates a query based on the conceptId and subsumption flavour (defaults to SubsumptionFlavour.SELF_OR_ANY_TYPE_OF).

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConstraintresourceApi;


ConstraintresourceApi apiInstance = new ConstraintresourceApi();
String id = "id_example"; // String | id
Map<String, String> flavour = new HashMap(); // Map<String, String> | flavour
try {
    TerminologyConstraint result = apiInstance.createConstraintUsingPOST(id, flavour);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConstraintresourceApi#createConstraintUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **flavour** | [**Map&lt;String, String&gt;**](String.md)| flavour | [optional]

### Return type

[**TerminologyConstraint**](TerminologyConstraint.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="deleteConstraintUsingDELETE"></a>
# **deleteConstraintUsingDELETE**
> deleteConstraintUsingDELETE(id)

deleteConstraint

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConstraintresourceApi;


ConstraintresourceApi apiInstance = new ConstraintresourceApi();
String id = "id_example"; // String | id
try {
    apiInstance.deleteConstraintUsingDELETE(id);
} catch (ApiException e) {
    System.err.println("Exception when calling ConstraintresourceApi#deleteConstraintUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getAllConstraintsUsingGET"></a>
# **getAllConstraintsUsingGET**
> List&lt;TerminologyConstraint&gt; getAllConstraintsUsingGET()

getAllConstraints

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConstraintresourceApi;


ConstraintresourceApi apiInstance = new ConstraintresourceApi();
try {
    List<TerminologyConstraint> result = apiInstance.getAllConstraintsUsingGET();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConstraintresourceApi#getAllConstraintsUsingGET");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;TerminologyConstraint&gt;**](TerminologyConstraint.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getConstraintUsingGET"></a>
# **getConstraintUsingGET**
> TerminologyConstraint getConstraintUsingGET(id)

getConstraint

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConstraintresourceApi;


ConstraintresourceApi apiInstance = new ConstraintresourceApi();
String id = "id_example"; // String | id
try {
    TerminologyConstraint result = apiInstance.getConstraintUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConstraintresourceApi#getConstraintUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |

### Return type

[**TerminologyConstraint**](TerminologyConstraint.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getSubsumedConceptCountForConstraintUsingGET"></a>
# **getSubsumedConceptCountForConstraintUsingGET**
> List&lt;Object&gt; getSubsumedConceptCountForConstraintUsingGET(id)

returns subsumed concepts count for constraint with a given uuid

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConstraintresourceApi;


ConstraintresourceApi apiInstance = new ConstraintresourceApi();
String id = "id_example"; // String | id
try {
    List<Object> result = apiInstance.getSubsumedConceptCountForConstraintUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConstraintresourceApi#getSubsumedConceptCountForConstraintUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |

### Return type

**List&lt;Object&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getSubsumedConceptsForConstraintUsingGET"></a>
# **getSubsumedConceptsForConstraintUsingGET**
> List&lt;Object&gt; getSubsumedConceptsForConstraintUsingGET(id)

returns subsumed concepts for constraint with a given uuid

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConstraintresourceApi;


ConstraintresourceApi apiInstance = new ConstraintresourceApi();
String id = "id_example"; // String | id
try {
    List<Object> result = apiInstance.getSubsumedConceptsForConstraintUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConstraintresourceApi#getSubsumedConceptsForConstraintUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |

### Return type

**List&lt;Object&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getSubsumptionForConstraintUsingGET"></a>
# **getSubsumptionForConstraintUsingGET**
> TerminologyConstraint getSubsumptionForConstraintUsingGET(id)

gets the subsumption flavour for constraint with id (defaults to SubsumptionFlavour.SELF_OR_ANY_TYPE_OF).

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConstraintresourceApi;


ConstraintresourceApi apiInstance = new ConstraintresourceApi();
String id = "id_example"; // String | id
try {
    TerminologyConstraint result = apiInstance.getSubsumptionForConstraintUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConstraintresourceApi#getSubsumptionForConstraintUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |

### Return type

[**TerminologyConstraint**](TerminologyConstraint.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="setSubsumptionForConstraintUsingPUT"></a>
# **setSubsumptionForConstraintUsingPUT**
> TerminologyConstraint setSubsumptionForConstraintUsingPUT(id, flavour)

updates the subsumption flavour for constraint with id (defaults to SubsumptionFlavour.SELF_OR_ANY_TYPE_OF).

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConstraintresourceApi;


ConstraintresourceApi apiInstance = new ConstraintresourceApi();
String id = "id_example"; // String | id
Object flavour = null; // Object | flavour
try {
    TerminologyConstraint result = apiInstance.setSubsumptionForConstraintUsingPUT(id, flavour);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConstraintresourceApi#setSubsumptionForConstraintUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **flavour** | **Object**| flavour |

### Return type

[**TerminologyConstraint**](TerminologyConstraint.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="updateConstraintUsingPUT"></a>
# **updateConstraintUsingPUT**
> TerminologyConstraint updateConstraintUsingPUT(id, content)

updates constraint with id

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.ConstraintresourceApi;


ConstraintresourceApi apiInstance = new ConstraintresourceApi();
String id = "id_example"; // String | id
Object content = null; // Object | content
try {
    TerminologyConstraint result = apiInstance.updateConstraintUsingPUT(id, content);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConstraintresourceApi#updateConstraintUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **content** | **Object**| content |

### Return type

[**TerminologyConstraint**](TerminologyConstraint.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

