# HierarchyresourceApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAncestorCountForIdUsingGET**](HierarchyresourceApi.md#getAncestorCountForIdUsingGET) | **GET** /api/hierarchy/{id}/counts/ancestors | get the ancestor count for concept with given id
[**getAncestorsForIdAsyncUsingGET**](HierarchyresourceApi.md#getAncestorsForIdAsyncUsingGET) | **GET** /api/hierarchy/version/{version}/id/{id}/anc | get the ancestors of concept with given id
[**getAncestorsForIdUsingGET**](HierarchyresourceApi.md#getAncestorsForIdUsingGET) | **GET** /api/hierarchy/version/{version}/id/{id}/ancestors | get the ancestors of concept with given id
[**getChildrenCountForIdUsingGET**](HierarchyresourceApi.md#getChildrenCountForIdUsingGET) | **GET** /api/hierarchy/version/{version}/id/{id}/counts/children | get the children count for concept with given id
[**getChildrenForIdUsingGET**](HierarchyresourceApi.md#getChildrenForIdUsingGET) | **GET** /api/hierarchy/version/{version}/id/{id}/children | get the children of concept with given id
[**getCountsForIdUsingGET**](HierarchyresourceApi.md#getCountsForIdUsingGET) | **GET** /api/hierarchy/version/{version}/id/{id}/counts | get an aggregate count of children, parent, ancestor and descendant counts for concept with given id
[**getDescendantCountForIdUsingGET**](HierarchyresourceApi.md#getDescendantCountForIdUsingGET) | **GET** /api/hierarchy/version/{version}/id/{id}/counts/descendants | gets the descendant count for concept with given id
[**getDescendantsForIdAsyncUsingGET**](HierarchyresourceApi.md#getDescendantsForIdAsyncUsingGET) | **GET** /api/hierarchy/version/{version}/id/{id}/desc | get the descendants of concept with given id
[**getDescendantsForIdUsingGET**](HierarchyresourceApi.md#getDescendantsForIdUsingGET) | **GET** /api/hierarchy/version/{version}/id/{id}/descendants | get the descendants of concept with given id
[**getParentCountForIdUsingGET**](HierarchyresourceApi.md#getParentCountForIdUsingGET) | **GET** /api/hierarchy/{id}/counts/parents | get the parent count for concept with given id
[**getParentsForIdUsingGET**](HierarchyresourceApi.md#getParentsForIdUsingGET) | **GET** /api/hierarchy/version/{version}/id/{id}/parents | get the parents of concept with given id
[**getProximalPrimitiveParentsForIdUsingGET**](HierarchyresourceApi.md#getProximalPrimitiveParentsForIdUsingGET) | **GET** /api/hierarchy/version/{version}/id/{id}/proximalparents | get the proximal primitive parents for concept with given id


<a name="getAncestorCountForIdUsingGET"></a>
# **getAncestorCountForIdUsingGET**
> Integer getAncestorCountForIdUsingGET(id, version)

get the ancestor count for concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.HierarchyresourceApi;


HierarchyresourceApi apiInstance = new HierarchyresourceApi();
String id = "id_example"; // String | id
String version = "version_example"; // String | The version of the terminology product as date string
try {
    Integer result = apiInstance.getAncestorCountForIdUsingGET(id, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HierarchyresourceApi#getAncestorCountForIdUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **version** | **String**| The version of the terminology product as date string |

### Return type

**Integer**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getAncestorsForIdAsyncUsingGET"></a>
# **getAncestorsForIdAsyncUsingGET**
> List&lt;Object&gt; getAncestorsForIdAsyncUsingGET(id, version, definitionStatus)

get the ancestors of concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.HierarchyresourceApi;


HierarchyresourceApi apiInstance = new HierarchyresourceApi();
String id = "id_example"; // String | id
String version = "version_example"; // String | The version of the terminology product as date string
String definitionStatus = "definitionStatus_example"; // String | definitionStatus
try {
    List<Object> result = apiInstance.getAncestorsForIdAsyncUsingGET(id, version, definitionStatus);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HierarchyresourceApi#getAncestorsForIdAsyncUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **version** | **String**| The version of the terminology product as date string |
 **definitionStatus** | **String**| definitionStatus | [optional] [enum: FULLY_DEFINED, PRIMITIVE, UNKNOWN]

### Return type

**List&lt;Object&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getAncestorsForIdUsingGET"></a>
# **getAncestorsForIdUsingGET**
> List&lt;Object&gt; getAncestorsForIdUsingGET(id, version, definitionStatus)

get the ancestors of concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.HierarchyresourceApi;


HierarchyresourceApi apiInstance = new HierarchyresourceApi();
String id = "id_example"; // String | id
String version = "version_example"; // String | The version of the terminology product as date string
String definitionStatus = "definitionStatus_example"; // String | definitionStatus
try {
    List<Object> result = apiInstance.getAncestorsForIdUsingGET(id, version, definitionStatus);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HierarchyresourceApi#getAncestorsForIdUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **version** | **String**| The version of the terminology product as date string |
 **definitionStatus** | **String**| definitionStatus | [optional] [enum: FULLY_DEFINED, PRIMITIVE, UNKNOWN]

### Return type

**List&lt;Object&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getChildrenCountForIdUsingGET"></a>
# **getChildrenCountForIdUsingGET**
> Integer getChildrenCountForIdUsingGET(id, version)

get the children count for concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.HierarchyresourceApi;


HierarchyresourceApi apiInstance = new HierarchyresourceApi();
String id = "id_example"; // String | id
String version = "version_example"; // String | The version of the terminology product as date string
try {
    Integer result = apiInstance.getChildrenCountForIdUsingGET(id, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HierarchyresourceApi#getChildrenCountForIdUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **version** | **String**| The version of the terminology product as date string |

### Return type

**Integer**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getChildrenForIdUsingGET"></a>
# **getChildrenForIdUsingGET**
> List&lt;Object&gt; getChildrenForIdUsingGET(id, version)

get the children of concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.HierarchyresourceApi;


HierarchyresourceApi apiInstance = new HierarchyresourceApi();
String id = "id_example"; // String | id
String version = "version_example"; // String | The version of the terminology product as date string
try {
    List<Object> result = apiInstance.getChildrenForIdUsingGET(id, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HierarchyresourceApi#getChildrenForIdUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **version** | **String**| The version of the terminology product as date string |

### Return type

**List&lt;Object&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getCountsForIdUsingGET"></a>
# **getCountsForIdUsingGET**
> Object getCountsForIdUsingGET(id, version)

get an aggregate count of children, parent, ancestor and descendant counts for concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.HierarchyresourceApi;


HierarchyresourceApi apiInstance = new HierarchyresourceApi();
String id = "id_example"; // String | id
String version = "version_example"; // String | The version of the terminology product as date string
try {
    Object result = apiInstance.getCountsForIdUsingGET(id, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HierarchyresourceApi#getCountsForIdUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **version** | **String**| The version of the terminology product as date string |

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getDescendantCountForIdUsingGET"></a>
# **getDescendantCountForIdUsingGET**
> Integer getDescendantCountForIdUsingGET(id, version)

gets the descendant count for concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.HierarchyresourceApi;


HierarchyresourceApi apiInstance = new HierarchyresourceApi();
String id = "id_example"; // String | id
String version = "version_example"; // String | The version of the terminology product as date string
try {
    Integer result = apiInstance.getDescendantCountForIdUsingGET(id, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HierarchyresourceApi#getDescendantCountForIdUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **version** | **String**| The version of the terminology product as date string |

### Return type

**Integer**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getDescendantsForIdAsyncUsingGET"></a>
# **getDescendantsForIdAsyncUsingGET**
> List&lt;Object&gt; getDescendantsForIdAsyncUsingGET(id, version)

get the descendants of concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.HierarchyresourceApi;


HierarchyresourceApi apiInstance = new HierarchyresourceApi();
String id = "id_example"; // String | id
String version = "version_example"; // String | The version of the terminology product as date string
try {
    List<Object> result = apiInstance.getDescendantsForIdAsyncUsingGET(id, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HierarchyresourceApi#getDescendantsForIdAsyncUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **version** | **String**| The version of the terminology product as date string |

### Return type

**List&lt;Object&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getDescendantsForIdUsingGET"></a>
# **getDescendantsForIdUsingGET**
> List&lt;Object&gt; getDescendantsForIdUsingGET(id, version)

get the descendants of concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.HierarchyresourceApi;


HierarchyresourceApi apiInstance = new HierarchyresourceApi();
String id = "id_example"; // String | id
String version = "version_example"; // String | The version of the terminology product as date string
try {
    List<Object> result = apiInstance.getDescendantsForIdUsingGET(id, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HierarchyresourceApi#getDescendantsForIdUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **version** | **String**| The version of the terminology product as date string |

### Return type

**List&lt;Object&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getParentCountForIdUsingGET"></a>
# **getParentCountForIdUsingGET**
> Integer getParentCountForIdUsingGET(id, version)

get the parent count for concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.HierarchyresourceApi;


HierarchyresourceApi apiInstance = new HierarchyresourceApi();
String id = "id_example"; // String | id
String version = "version_example"; // String | The version of the terminology product as date string
try {
    Integer result = apiInstance.getParentCountForIdUsingGET(id, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HierarchyresourceApi#getParentCountForIdUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **version** | **String**| The version of the terminology product as date string |

### Return type

**Integer**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getParentsForIdUsingGET"></a>
# **getParentsForIdUsingGET**
> List&lt;Object&gt; getParentsForIdUsingGET(id, version, definitionStatus)

get the parents of concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.HierarchyresourceApi;


HierarchyresourceApi apiInstance = new HierarchyresourceApi();
String id = "id_example"; // String | id
String version = "version_example"; // String | The version of the terminology product as date string
String definitionStatus = "definitionStatus_example"; // String | definitionStatus
try {
    List<Object> result = apiInstance.getParentsForIdUsingGET(id, version, definitionStatus);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HierarchyresourceApi#getParentsForIdUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **version** | **String**| The version of the terminology product as date string |
 **definitionStatus** | **String**| definitionStatus | [optional] [enum: FULLY_DEFINED, PRIMITIVE, UNKNOWN]

### Return type

**List&lt;Object&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

<a name="getProximalPrimitiveParentsForIdUsingGET"></a>
# **getProximalPrimitiveParentsForIdUsingGET**
> List&lt;Object&gt; getProximalPrimitiveParentsForIdUsingGET(id, version)

get the proximal primitive parents for concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.HierarchyresourceApi;


HierarchyresourceApi apiInstance = new HierarchyresourceApi();
String id = "id_example"; // String | id
String version = "version_example"; // String | The version of the terminology product as date string
try {
    List<Object> result = apiInstance.getProximalPrimitiveParentsForIdUsingGET(id, version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HierarchyresourceApi#getProximalPrimitiveParentsForIdUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **version** | **String**| The version of the terminology product as date string |

### Return type

**List&lt;Object&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: *_/_*

