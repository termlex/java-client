# SearchcontrollerApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**searchRefsetsUsingGET**](SearchcontrollerApi.md#searchRefsetsUsingGET) | **GET** /api/search/refsets | Returns matches for given token for a specified version of SNOMED CT as a list of map objects
[**searchSnomedUsingGET**](SearchcontrollerApi.md#searchSnomedUsingGET) | **GET** /api/search/sct | Returns matches for given token for a specified version of SNOMED CT as a list of map objects


<a name="searchRefsetsUsingGET"></a>
# **searchRefsetsUsingGET**
> Object searchRefsetsUsingGET(term, includedRefsetIds, maxResultsSize, start, conceptTypes, includeFSNs, includeInactive, edition, version, includedMembers, excludedMembers, excludedRefsetIds, languageCode)

Returns matches for given token for a specified version of SNOMED CT as a list of map objects

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.SearchcontrollerApi;


SearchcontrollerApi apiInstance = new SearchcontrollerApi();
String term = "term_example"; // String | Token to search for; can be a term or an id
List<String> includedRefsetIds = Arrays.asList("includedRefsetIds_example"); // List<String> | The ids of refsets used to limit the search results
Integer maxResultsSize = 100; // Integer | Maximum number of results to return
Integer start = 0; // Integer | The start position of search results to return
List<String> conceptTypes = Arrays.asList("conceptTypes_example"); // List<String> | The concept hierarchies/types to filter results to
Boolean includeFSNs = false; // Boolean | Toggle inclusion of FSNs in results
Boolean includeInactive = false; // Boolean | Toggle inclusion of inactive concepts in results
String edition = "int"; // String | The nam of the SNOMED CT edition to search against
String version = "version_example"; // String | The version of the terminology product as date string. Defaults to default version specified in server instance
List<String> includedMembers = Arrays.asList("includedMembers_example"); // List<String> | The concepts to include in search results
List<String> excludedMembers = Arrays.asList("excludedMembers_example"); // List<String> | The concepts to exclude from search results
List<String> excludedRefsetIds = Arrays.asList("excludedRefsetIds_example"); // List<String> | The ids of refsets to exclude from search results
String languageCode = "en"; // String | The language of descriptions to search
try {
    Object result = apiInstance.searchRefsetsUsingGET(term, includedRefsetIds, maxResultsSize, start, conceptTypes, includeFSNs, includeInactive, edition, version, includedMembers, excludedMembers, excludedRefsetIds, languageCode);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SearchcontrollerApi#searchRefsetsUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **term** | **String**| Token to search for; can be a term or an id |
 **includedRefsetIds** | [**List&lt;String&gt;**](String.md)| The ids of refsets used to limit the search results |
 **maxResultsSize** | **Integer**| Maximum number of results to return | [optional] [default to 100]
 **start** | **Integer**| The start position of search results to return | [optional] [default to 0]
 **conceptTypes** | [**List&lt;String&gt;**](String.md)| The concept hierarchies/types to filter results to | [optional]
 **includeFSNs** | **Boolean**| Toggle inclusion of FSNs in results | [optional] [default to false]
 **includeInactive** | **Boolean**| Toggle inclusion of inactive concepts in results | [optional] [default to false]
 **edition** | **String**| The nam of the SNOMED CT edition to search against | [optional] [default to int]
 **version** | **String**| The version of the terminology product as date string. Defaults to default version specified in server instance | [optional]
 **includedMembers** | [**List&lt;String&gt;**](String.md)| The concepts to include in search results | [optional]
 **excludedMembers** | [**List&lt;String&gt;**](String.md)| The concepts to exclude from search results | [optional]
 **excludedRefsetIds** | [**List&lt;String&gt;**](String.md)| The ids of refsets to exclude from search results | [optional]
 **languageCode** | **String**| The language of descriptions to search | [optional] [default to en]

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="searchSnomedUsingGET"></a>
# **searchSnomedUsingGET**
> Object searchSnomedUsingGET(term, maxResultsSize, start, conceptTypes, includeFSNs, includeInactive, edition, version, includedMembers, excludedMembers, excludedRefsetIds, languageCode)

Returns matches for given token for a specified version of SNOMED CT as a list of map objects

### Example
```java
// Import classes:
//import com.termlex.model.handler.ApiException;
//import com.termlex.client.SearchcontrollerApi;


SearchcontrollerApi apiInstance = new SearchcontrollerApi();
String term = "term_example"; // String | Token to search for; can be a term or an id
Integer maxResultsSize = 100; // Integer | Maximum number of results to return
Integer start = 0; // Integer | The start position of search results to return
List<String> conceptTypes = Arrays.asList("conceptTypes_example"); // List<String> | The concept hierarchies/types to filter results to
Boolean includeFSNs = false; // Boolean | Toggle inclusion of FSNs in results
Boolean includeInactive = false; // Boolean | Toggle inclusion of inactive concepts in results
String edition = "int"; // String | The name of the SNOMED CT edition to search against
String version = "version_example"; // String | The version of the terminology product as date string. Defaults to default version specified in server instance
List<String> includedMembers = Arrays.asList("includedMembers_example"); // List<String> | The concepts to include in search results
List<String> excludedMembers = Arrays.asList("excludedMembers_example"); // List<String> | The concepts to exclude from search results
List<String> excludedRefsetIds = Arrays.asList("excludedRefsetIds_example"); // List<String> | The ids of refsets to exclude from search results
String languageCode = "en"; // String | The language of descriptions to search
try {
    Object result = apiInstance.searchSnomedUsingGET(term, maxResultsSize, start, conceptTypes, includeFSNs, includeInactive, edition, version, includedMembers, excludedMembers, excludedRefsetIds, languageCode);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SearchcontrollerApi#searchSnomedUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **term** | **String**| Token to search for; can be a term or an id |
 **maxResultsSize** | **Integer**| Maximum number of results to return | [optional] [default to 100]
 **start** | **Integer**| The start position of search results to return | [optional] [default to 0]
 **conceptTypes** | [**List&lt;String&gt;**](String.md)| The concept hierarchies/types to filter results to | [optional]
 **includeFSNs** | **Boolean**| Toggle inclusion of FSNs in results | [optional] [default to false]
 **includeInactive** | **Boolean**| Toggle inclusion of inactive concepts in results | [optional] [default to false]
 **edition** | **String**| The name of the SNOMED CT edition to search against | [optional] [default to int]
 **version** | **String**| The version of the terminology product as date string. Defaults to default version specified in server instance | [optional]
 **includedMembers** | [**List&lt;String&gt;**](String.md)| The concepts to include in search results | [optional]
 **excludedMembers** | [**List&lt;String&gt;**](String.md)| The concepts to exclude from search results | [optional]
 **excludedRefsetIds** | [**List&lt;String&gt;**](String.md)| The ids of refsets to exclude from search results | [optional]
 **languageCode** | **String**| The language of descriptions to search | [optional] [default to en]

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

