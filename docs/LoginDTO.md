
# LoginDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**password** | **String** |  |  [optional]
**rememberMe** | **Boolean** |  |  [optional]
**username** | **String** |  |  [optional]



