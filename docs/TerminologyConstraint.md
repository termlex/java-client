
# TerminologyConstraint

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cgf** | **String** |  |  [optional]
**dimensionVocabulary** | [**DimensionVocabularyEnum**](#DimensionVocabularyEnum) |  |  [optional]
**expression** | [**Expression**](Expression.md) |  |  [optional]
**expressionUuid** | **String** |  |  [optional]
**id** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**precoordinated** | **Boolean** |  |  [optional]
**subsumption** | [**SubsumptionEnum**](#SubsumptionEnum) |  |  [optional]
**type** | [**TypeEnum**](#TypeEnum) |  |  [optional]
**uuid** | **String** |  |  [optional]
**value** | [**ConstraintValueOfobject**](ConstraintValueOfobject.md) |  |  [optional]


<a name="DimensionVocabularyEnum"></a>
## Enum: DimensionVocabularyEnum
Name | Value
---- | -----
SECOND | &quot;SECOND&quot;
HOUR | &quot;HOUR&quot;
DAY | &quot;DAY&quot;
WEEK | &quot;WEEK&quot;
MONTH | &quot;MONTH&quot;
YEAR | &quot;YEAR&quot;
NULL | &quot;NULL&quot;
DATE | &quot;DATE&quot;


<a name="SubsumptionEnum"></a>
## Enum: SubsumptionEnum
Name | Value
---- | -----
SELF_OR_ANY_TYPE_OF | &quot;SELF_OR_ANY_TYPE_OF&quot;
SELF_ONLY | &quot;SELF_ONLY&quot;
ANY_TYPE_OF_BUT_NOT_SELF | &quot;ANY_TYPE_OF_BUT_NOT_SELF&quot;
NOT_SELF | &quot;NOT_SELF&quot;
NOT_SELF_OR_ANY_TYPE_OF | &quot;NOT_SELF_OR_ANY_TYPE_OF&quot;
UNKNOWN | &quot;UNKNOWN&quot;


<a name="TypeEnum"></a>
## Enum: TypeEnum
Name | Value
---- | -----
DATA | &quot;DATA&quot;
TEMPORAL | &quot;TEMPORAL&quot;
TERMINOLOGY | &quot;TERMINOLOGY&quot;



