
# TerminologyConceptImpl

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createdBy** | **String** |  |  [optional]
**localId** | **String** |  |  [optional]
**updatedBy** | **String** |  |  [optional]



