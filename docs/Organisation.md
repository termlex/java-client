
# Organisation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**codeSystems** | [**List&lt;CodeSystem&gt;**](CodeSystem.md) |  |  [optional]
**contactEmail** | **String** |  |  [optional]
**id** | **Long** |  |  [optional]
**name** | **String** |  |  [optional]



