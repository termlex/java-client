# java-client

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
    <groupId>com.termlex</groupId>
    <artifactId>java-client</artifactId>
    <version>1.0.0</version>
    <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "com.termlex:java-client:1.0.0"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/java-client-1.0.0.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import com.termlex.model.handler.*;
import com.termlex.model.handler.auth.*;
import com.termlex.model.handler.model.*;
import com.termlex.client.AccountresourceApi;

import java.io.File;
import java.util.*;

public class AccountresourceApiExample {

    public static void main(String[] args) {
        
        AccountresourceApi apiInstance = new AccountresourceApi();
        String key = "key_example"; // String | key
        try {
            String result = apiInstance.activateAccountUsingGET(key);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling AccountresourceApi#activateAccountUsingGET");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *https://localhost/*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AccountresourceApi* | [**activateAccountUsingGET**](docs/AccountresourceApi.md#activateAccountUsingGET) | **GET** /api/activate | activateAccount
*AccountresourceApi* | [**authorizeUsingPOST**](docs/AccountresourceApi.md#authorizeUsingPOST) | **POST** /api/authenticate | authorize
*AccountresourceApi* | [**changePasswordUsingPOST**](docs/AccountresourceApi.md#changePasswordUsingPOST) | **POST** /api/account/change_password | changePassword
*AccountresourceApi* | [**finishPasswordResetUsingPOST**](docs/AccountresourceApi.md#finishPasswordResetUsingPOST) | **POST** /api/account/reset_password/finish | finishPasswordReset
*AccountresourceApi* | [**getAccountUsingGET**](docs/AccountresourceApi.md#getAccountUsingGET) | **GET** /api/account | getAccount
*AccountresourceApi* | [**isAuthenticatedUsingGET**](docs/AccountresourceApi.md#isAuthenticatedUsingGET) | **GET** /api/authenticate | isAuthenticated
*AccountresourceApi* | [**registerAccountUsingPOST**](docs/AccountresourceApi.md#registerAccountUsingPOST) | **POST** /api/register | registerAccount
*AccountresourceApi* | [**requestPasswordResetUsingPOST**](docs/AccountresourceApi.md#requestPasswordResetUsingPOST) | **POST** /api/account/reset_password/init | requestPasswordReset
*AccountresourceApi* | [**saveAccountUsingPOST**](docs/AccountresourceApi.md#saveAccountUsingPOST) | **POST** /api/account | saveAccount
*ConceptresourceApi* | [**createConceptUsingPOST**](docs/ConceptresourceApi.md#createConceptUsingPOST) | **POST** /api/concepts | createConcept
*ConceptresourceApi* | [**deleteConceptUsingDELETE**](docs/ConceptresourceApi.md#deleteConceptUsingDELETE) | **DELETE** /api/concepts/{id} | deleteConcept
*ConceptresourceApi* | [**getAllConceptsUsingGET**](docs/ConceptresourceApi.md#getAllConceptsUsingGET) | **GET** /api/concepts | getAllConcepts
*ConceptresourceApi* | [**getComponentStatusForIdUsingGET**](docs/ConceptresourceApi.md#getComponentStatusForIdUsingGET) | **GET** /api/concepts/version/{version}/id/{id}/status | get the status of concept with given id
*ConceptresourceApi* | [**getCompositionalGrammarFormForIdUsingGET**](docs/ConceptresourceApi.md#getCompositionalGrammarFormForIdUsingGET) | **GET** /api/concepts/version/{version}/id/{id}/cgf | get the compositional grammar form of concept with given id
*ConceptresourceApi* | [**getConceptForIdUsingGET**](docs/ConceptresourceApi.md#getConceptForIdUsingGET) | **GET** /api/concepts/version/{version}/id/{id} | get the concept with given id
*ConceptresourceApi* | [**getConceptFullySpecifiedNameForIdUsingGET**](docs/ConceptresourceApi.md#getConceptFullySpecifiedNameForIdUsingGET) | **GET** /api/concepts/version/{version}/id/{id}/fsn | get the Fully Specified Name of concept with given id and language
*ConceptresourceApi* | [**getConceptPreferredTermForIdUsingGET**](docs/ConceptresourceApi.md#getConceptPreferredTermForIdUsingGET) | **GET** /api/concepts/version/{version}/id/{id}/pt | get the Preferred Term of concept with given id
*ConceptresourceApi* | [**getConceptTypeForIdUsingGET**](docs/ConceptresourceApi.md#getConceptTypeForIdUsingGET) | **GET** /api/concepts/version/{version}/id/{id}/type | get the type of concept with given id
*ConceptresourceApi* | [**getConceptUsingGET**](docs/ConceptresourceApi.md#getConceptUsingGET) | **GET** /api/concepts/{id} | getConcept
*ConceptresourceApi* | [**getDeltaContentUsingGET**](docs/ConceptresourceApi.md#getDeltaContentUsingGET) | **GET** /api/concepts/version/{version}/delta/all | get all delta content for the release
*ConceptresourceApi* | [**getLongNormalFormForIdUsingGET**](docs/ConceptresourceApi.md#getLongNormalFormForIdUsingGET) | **GET** /api/concepts/version/{version}/id/{id}/lnf | get the long normal form of concept with given id
*ConceptresourceApi* | [**getNewlyAddedContentUsingGET**](docs/ConceptresourceApi.md#getNewlyAddedContentUsingGET) | **GET** /api/concepts/version/{version}/delta/new | get newly added content for the release
*ConceptresourceApi* | [**getNewlyModifiedContentUsingGET**](docs/ConceptresourceApi.md#getNewlyModifiedContentUsingGET) | **GET** /api/concepts/version/{version}/delta/modified | get still active concepts modified in the release
*ConceptresourceApi* | [**getNewlyResurrectedContentUsingGET**](docs/ConceptresourceApi.md#getNewlyResurrectedContentUsingGET) | **GET** /api/concepts/version/{version}/delta/resurrected | get newly resurrected content for the release
*ConceptresourceApi* | [**getNewlyRetiredContentUsingGET**](docs/ConceptresourceApi.md#getNewlyRetiredContentUsingGET) | **GET** /api/concepts/version/{version}/delta/retired | get newly retired content for the release
*ConceptresourceApi* | [**getProspectiveReplacementsForIdUsingGET**](docs/ConceptresourceApi.md#getProspectiveReplacementsForIdUsingGET) | **GET** /api/concepts/version/{version}/id/{id}/history/prospective | get the prospective replacement map for concept with given id
*ConceptresourceApi* | [**getRetrospectiveReplacementsForIdUsingGET**](docs/ConceptresourceApi.md#getRetrospectiveReplacementsForIdUsingGET) | **GET** /api/concepts/version/{version}/id/{id}/history/retroprospective | get the retrospective replacement map concept with given id
*ConceptresourceApi* | [**getShortNormalFormForIdUsingGET**](docs/ConceptresourceApi.md#getShortNormalFormForIdUsingGET) | **GET** /api/concepts/version/{version}/id/{id}/snf | get the short normal form of concept with given id
*ConceptresourceApi* | [**getSubsumptionRelationshipUsingGET**](docs/ConceptresourceApi.md#getSubsumptionRelationshipUsingGET) | **GET** /api/concepts/compare | get the subsumption relationship of comparing two concepts
*ConceptresourceApi* | [**updateConceptUsingPUT**](docs/ConceptresourceApi.md#updateConceptUsingPUT) | **PUT** /api/concepts | updateConcept
*ConstraintresourceApi* | [**createConstraintUsingPOST**](docs/ConstraintresourceApi.md#createConstraintUsingPOST) | **POST** /api/constraints/{id} | creates a query based on the conceptId and subsumption flavour (defaults to SubsumptionFlavour.SELF_OR_ANY_TYPE_OF).
*ConstraintresourceApi* | [**deleteConstraintUsingDELETE**](docs/ConstraintresourceApi.md#deleteConstraintUsingDELETE) | **DELETE** /api/constraints/{id} | deleteConstraint
*ConstraintresourceApi* | [**getAllConstraintsUsingGET**](docs/ConstraintresourceApi.md#getAllConstraintsUsingGET) | **GET** /api/constraints | getAllConstraints
*ConstraintresourceApi* | [**getConstraintUsingGET**](docs/ConstraintresourceApi.md#getConstraintUsingGET) | **GET** /api/constraints/{id} | getConstraint
*ConstraintresourceApi* | [**getSubsumedConceptCountForConstraintUsingGET**](docs/ConstraintresourceApi.md#getSubsumedConceptCountForConstraintUsingGET) | **GET** /api/constraints/{id}/conceptcount | returns subsumed concepts count for constraint with a given uuid
*ConstraintresourceApi* | [**getSubsumedConceptsForConstraintUsingGET**](docs/ConstraintresourceApi.md#getSubsumedConceptsForConstraintUsingGET) | **GET** /api/constraints/{id}/concepts | returns subsumed concepts for constraint with a given uuid
*ConstraintresourceApi* | [**getSubsumptionForConstraintUsingGET**](docs/ConstraintresourceApi.md#getSubsumptionForConstraintUsingGET) | **GET** /api/constraints/{id}/flavour | gets the subsumption flavour for constraint with id (defaults to SubsumptionFlavour.SELF_OR_ANY_TYPE_OF).
*ConstraintresourceApi* | [**setSubsumptionForConstraintUsingPUT**](docs/ConstraintresourceApi.md#setSubsumptionForConstraintUsingPUT) | **PUT** /api/constraints/{id}/flavour | updates the subsumption flavour for constraint with id (defaults to SubsumptionFlavour.SELF_OR_ANY_TYPE_OF).
*ConstraintresourceApi* | [**updateConstraintUsingPUT**](docs/ConstraintresourceApi.md#updateConstraintUsingPUT) | **PUT** /api/constraints/{id} | updates constraint with id
*ExpressionresourceApi* | [**getCompositionalGrammarFormUsingGET**](docs/ExpressionresourceApi.md#getCompositionalGrammarFormUsingGET) | **GET** /api/expressions/cgf | get the compositional grammar form of expression
*ExpressionresourceApi* | [**getLongNormalFormUsingGET**](docs/ExpressionresourceApi.md#getLongNormalFormUsingGET) | **GET** /api/expressions/lnf | get the long normal form of expression
*ExpressionresourceApi* | [**getShortNormalFormUsingGET**](docs/ExpressionresourceApi.md#getShortNormalFormUsingGET) | **GET** /api/expressions/snf | get the long normal form of expression
*ExpressionresourceApi* | [**getSubsumptionRelationshipUsingGET1**](docs/ExpressionresourceApi.md#getSubsumptionRelationshipUsingGET1) | **GET** /api/expressions/compare | get the subsumption relationship of comparing two expressions
*HierarchyresourceApi* | [**getAncestorCountForIdUsingGET**](docs/HierarchyresourceApi.md#getAncestorCountForIdUsingGET) | **GET** /api/hierarchy/{id}/counts/ancestors | get the ancestor count for concept with given id
*HierarchyresourceApi* | [**getAncestorsForIdAsyncUsingGET**](docs/HierarchyresourceApi.md#getAncestorsForIdAsyncUsingGET) | **GET** /api/hierarchy/version/{version}/id/{id}/anc | get the ancestors of concept with given id
*HierarchyresourceApi* | [**getAncestorsForIdUsingGET**](docs/HierarchyresourceApi.md#getAncestorsForIdUsingGET) | **GET** /api/hierarchy/version/{version}/id/{id}/ancestors | get the ancestors of concept with given id
*HierarchyresourceApi* | [**getChildrenCountForIdUsingGET**](docs/HierarchyresourceApi.md#getChildrenCountForIdUsingGET) | **GET** /api/hierarchy/version/{version}/id/{id}/counts/children | get the children count for concept with given id
*HierarchyresourceApi* | [**getChildrenForIdUsingGET**](docs/HierarchyresourceApi.md#getChildrenForIdUsingGET) | **GET** /api/hierarchy/version/{version}/id/{id}/children | get the children of concept with given id
*HierarchyresourceApi* | [**getCountsForIdUsingGET**](docs/HierarchyresourceApi.md#getCountsForIdUsingGET) | **GET** /api/hierarchy/version/{version}/id/{id}/counts | get an aggregate count of children, parent, ancestor and descendant counts for concept with given id
*HierarchyresourceApi* | [**getDescendantCountForIdUsingGET**](docs/HierarchyresourceApi.md#getDescendantCountForIdUsingGET) | **GET** /api/hierarchy/version/{version}/id/{id}/counts/descendants | gets the descendant count for concept with given id
*HierarchyresourceApi* | [**getDescendantsForIdAsyncUsingGET**](docs/HierarchyresourceApi.md#getDescendantsForIdAsyncUsingGET) | **GET** /api/hierarchy/version/{version}/id/{id}/desc | get the descendants of concept with given id
*HierarchyresourceApi* | [**getDescendantsForIdUsingGET**](docs/HierarchyresourceApi.md#getDescendantsForIdUsingGET) | **GET** /api/hierarchy/version/{version}/id/{id}/descendants | get the descendants of concept with given id
*HierarchyresourceApi* | [**getParentCountForIdUsingGET**](docs/HierarchyresourceApi.md#getParentCountForIdUsingGET) | **GET** /api/hierarchy/{id}/counts/parents | get the parent count for concept with given id
*HierarchyresourceApi* | [**getParentsForIdUsingGET**](docs/HierarchyresourceApi.md#getParentsForIdUsingGET) | **GET** /api/hierarchy/version/{version}/id/{id}/parents | get the parents of concept with given id
*HierarchyresourceApi* | [**getProximalPrimitiveParentsForIdUsingGET**](docs/HierarchyresourceApi.md#getProximalPrimitiveParentsForIdUsingGET) | **GET** /api/hierarchy/version/{version}/id/{id}/proximalparents | get the proximal primitive parents for concept with given id
*ProductresourceApi* | [**createProductUsingPOST**](docs/ProductresourceApi.md#createProductUsingPOST) | **POST** /api/products | createProduct
*ProductresourceApi* | [**deleteProductUsingDELETE**](docs/ProductresourceApi.md#deleteProductUsingDELETE) | **DELETE** /api/products/{id} | deleteProduct
*ProductresourceApi* | [**getAllProductsUsingGET**](docs/ProductresourceApi.md#getAllProductsUsingGET) | **GET** /api/products | getAllProducts
*ProductresourceApi* | [**getProductUsingGET**](docs/ProductresourceApi.md#getProductUsingGET) | **GET** /api/products/{id} | getProduct
*ProductresourceApi* | [**getProductVersionsUsingGET**](docs/ProductresourceApi.md#getProductVersionsUsingGET) | **GET** /api/products/{id}/versions | getProductVersions
*ProductresourceApi* | [**updateProductUsingPUT**](docs/ProductresourceApi.md#updateProductUsingPUT) | **PUT** /api/products | updateProduct
*QueryresourceApi* | [**addConstraintInQueryUsingPOST**](docs/QueryresourceApi.md#addConstraintInQueryUsingPOST) | **POST** /api/queries/{id}/constraints/included | Sets the included constraint for a query component expression.
*QueryresourceApi* | [**addContainedExpressionsForQueryUsingPUT**](docs/QueryresourceApi.md#addContainedExpressionsForQueryUsingPUT) | **PUT** /api/queries/{id}/children | adds child expressions to query with a given uuid
*QueryresourceApi* | [**addExcludedConstraintsInQueryUsingPUT**](docs/QueryresourceApi.md#addExcludedConstraintsInQueryUsingPUT) | **PUT** /api/queries/{id}/constraints/excluded | Adds the excluded constraints to a query component expression.
*QueryresourceApi* | [**createQueryUsingPOST**](docs/QueryresourceApi.md#createQueryUsingPOST) | **POST** /api/queries/{type} | creates a query based on the type requested (defaults to QueryComponent).
*QueryresourceApi* | [**createQueryUsingPOST1**](docs/QueryresourceApi.md#createQueryUsingPOST1) | **POST** /api/queries | creates a query based on the type requested (defaults to QueryComponent).
*QueryresourceApi* | [**deleteConstraintInQueryComponentUsingDELETE**](docs/QueryresourceApi.md#deleteConstraintInQueryComponentUsingDELETE) | **DELETE** /api/queries/{id}/constraints/included | Deletes the included constraint for a query component expression.
*QueryresourceApi* | [**deleteQueryUsingDELETE**](docs/QueryresourceApi.md#deleteQueryUsingDELETE) | **DELETE** /api/queries/{id} | deleteQuery
*QueryresourceApi* | [**getAllQueriesUsingGET**](docs/QueryresourceApi.md#getAllQueriesUsingGET) | **GET** /api/queries | getAllQueries
*QueryresourceApi* | [**getConstraintInQueryUsingGET**](docs/QueryresourceApi.md#getConstraintInQueryUsingGET) | **GET** /api/queries/{id}/constraints/included | Gets the included constraint for a query component expression.
*QueryresourceApi* | [**getConstraintsInQueryUsingGET**](docs/QueryresourceApi.md#getConstraintsInQueryUsingGET) | **GET** /api/queries/{id}/constraints | Gets the included and excluded constraints for a query component expression.
*QueryresourceApi* | [**getContainedExpressionsForQueryUsingGET**](docs/QueryresourceApi.md#getContainedExpressionsForQueryUsingGET) | **GET** /api/queries/{id}/children | returns contained child expressions for query with a given uuid
*QueryresourceApi* | [**getExcludedConstraintsInQueryUsingGET**](docs/QueryresourceApi.md#getExcludedConstraintsInQueryUsingGET) | **GET** /api/queries/{id}/constraints/excluded | Gets the excluded constraints for a query component expression.
*QueryresourceApi* | [**getQueryUsingGET**](docs/QueryresourceApi.md#getQueryUsingGET) | **GET** /api/queries/{id} | getQuery
*QueryresourceApi* | [**getSubsumedConceptsCountForQueryUsingGET**](docs/QueryresourceApi.md#getSubsumedConceptsCountForQueryUsingGET) | **GET** /api/queries/{id}/conceptcount | returns subsumed concepts count for query for a given uuid
*QueryresourceApi* | [**getSubsumedConceptsForQueryUsingGET**](docs/QueryresourceApi.md#getSubsumedConceptsForQueryUsingGET) | **GET** /api/queries/{id}/concepts | returns subsumed concepts for query for a given uuid
*QueryresourceApi* | [**removeContainedExpressionsForQueryUsingDELETE**](docs/QueryresourceApi.md#removeContainedExpressionsForQueryUsingDELETE) | **DELETE** /api/queries/{id}/children | removes child expressions from query with a given uuid
*QueryresourceApi* | [**removeExcludedConstraintsInQueryUsingDELETE**](docs/QueryresourceApi.md#removeExcludedConstraintsInQueryUsingDELETE) | **DELETE** /api/queries/{id}/constraints/excluded | Removes the excluded constraints from a query component expression.
*QueryresourceApi* | [**setContainedExpressionsForQueryUsingPOST**](docs/QueryresourceApi.md#setContainedExpressionsForQueryUsingPOST) | **POST** /api/queries/{id}/children | sets contained child expressions for query with a given uuid
*QueryresourceApi* | [**setExcludedConstraintsInQueryUsingPOST**](docs/QueryresourceApi.md#setExcludedConstraintsInQueryUsingPOST) | **POST** /api/queries/{id}/constraints/excluded | Sets the excluded constraints for a query component expression.
*QueryresourceApi* | [**updateConstraintInQueryComponentUsingPUT**](docs/QueryresourceApi.md#updateConstraintInQueryComponentUsingPUT) | **PUT** /api/queries/{id}/constraints/included | Updates the included constraint for a query component expression.
*QueryresourceApi* | [**updateQueryUsingPUT**](docs/QueryresourceApi.md#updateQueryUsingPUT) | **PUT** /api/queries/{id} | updates query with id
*RefsetresourceApi* | [**addDefinitionsUsingPUT**](docs/RefsetresourceApi.md#addDefinitionsUsingPUT) | **PUT** /api/refsets/{id}/definitions | adds definitions to refset with given id. This does not clear existing definitions.
*RefsetresourceApi* | [**addMembersUsingPUT**](docs/RefsetresourceApi.md#addMembersUsingPUT) | **PUT** /api/refsets/{id}/members | adds collection as members of refset with given id. This does not clear existing members.
*RefsetresourceApi* | [**createDefinitionsUsingPOST**](docs/RefsetresourceApi.md#createDefinitionsUsingPOST) | **POST** /api/refsets/{id}/definitions | sets collection as definitions of refset with given id. Note this clears existing definitions.
*RefsetresourceApi* | [**createMembersUsingPOST**](docs/RefsetresourceApi.md#createMembersUsingPOST) | **POST** /api/refsets/{id}/members | set collection as members of refset with given id. Note this clears existing members.
*RefsetresourceApi* | [**createRefsetUsingPOST**](docs/RefsetresourceApi.md#createRefsetUsingPOST) | **POST** /api/refsets | createRefset
*RefsetresourceApi* | [**deleteRefsetUsingDELETE**](docs/RefsetresourceApi.md#deleteRefsetUsingDELETE) | **DELETE** /api/refsets/{id} | deleteRefset
*RefsetresourceApi* | [**getAllRefsetsUsingGET**](docs/RefsetresourceApi.md#getAllRefsetsUsingGET) | **GET** /api/refsets | getAllRefsets
*RefsetresourceApi* | [**getRefsetBySctIdUsingGET**](docs/RefsetresourceApi.md#getRefsetBySctIdUsingGET) | **GET** /api/refsets/sct/{id} | returns refset for a given SNOMED CT id
*RefsetresourceApi* | [**getRefsetByUuidUsingGET**](docs/RefsetresourceApi.md#getRefsetByUuidUsingGET) | **GET** /api/refsets/uuid/{id} | returns refset for a given uuid
*RefsetresourceApi* | [**getRefsetDefinitionsUsingGET**](docs/RefsetresourceApi.md#getRefsetDefinitionsUsingGET) | **GET** /api/refsets/{id}/definitions | returns definitions of refset with given id
*RefsetresourceApi* | [**getRefsetMemberCountUsingGET**](docs/RefsetresourceApi.md#getRefsetMemberCountUsingGET) | **GET** /api/refsets/{id}/members/count | returns member count for refset with given id
*RefsetresourceApi* | [**getRefsetMembersUsingGET**](docs/RefsetresourceApi.md#getRefsetMembersUsingGET) | **GET** /api/refsets/{id}/members | returns members of refset with given id
*RefsetresourceApi* | [**getRefsetUsingGET**](docs/RefsetresourceApi.md#getRefsetUsingGET) | **GET** /api/refsets/{id} | getRefset
*RefsetresourceApi* | [**getRefsetsInModuleUsingGET**](docs/RefsetresourceApi.md#getRefsetsInModuleUsingGET) | **GET** /api/refsets/module/{id} | gets all refsets that belong to module with a given id
*RefsetresourceApi* | [**removeDefinitionsUsingDELETE**](docs/RefsetresourceApi.md#removeDefinitionsUsingDELETE) | **DELETE** /api/refsets/{id}/definitions | deletes definitions from refset with given id
*RefsetresourceApi* | [**removeMembersUsingDELETE**](docs/RefsetresourceApi.md#removeMembersUsingDELETE) | **DELETE** /api/refsets/{id}/members | removes collection from members of refset with given id
*RefsetresourceApi* | [**searchRefsetUsingGET**](docs/RefsetresourceApi.md#searchRefsetUsingGET) | **GET** /api/refsets/{id}/version/{version}/search | Returns matches in a refset for given token
*RefsetresourceApi* | [**updateRefsetUsingPUT**](docs/RefsetresourceApi.md#updateRefsetUsingPUT) | **PUT** /api/refsets | updateRefset
*SearchcontrollerApi* | [**searchRefsetsUsingGET**](docs/SearchcontrollerApi.md#searchRefsetsUsingGET) | **GET** /api/search/refsets | Returns matches for given token for a specified version of SNOMED CT as a list of map objects
*SearchcontrollerApi* | [**searchSnomedUsingGET**](docs/SearchcontrollerApi.md#searchSnomedUsingGET) | **GET** /api/search/sct | Returns matches for given token for a specified version of SNOMED CT as a list of map objects
*UserresourceApi* | [**createUserUsingPOST**](docs/UserresourceApi.md#createUserUsingPOST) | **POST** /api/users | createUser
*UserresourceApi* | [**deleteUserUsingDELETE**](docs/UserresourceApi.md#deleteUserUsingDELETE) | **DELETE** /api/users/{login} | deleteUser
*UserresourceApi* | [**getAllUsersUsingGET**](docs/UserresourceApi.md#getAllUsersUsingGET) | **GET** /api/users | getAllUsers
*UserresourceApi* | [**getUserUsingGET**](docs/UserresourceApi.md#getUserUsingGET) | **GET** /api/users/{login} | getUser
*UserresourceApi* | [**updateUserUsingPUT**](docs/UserresourceApi.md#updateUserUsingPUT) | **PUT** /api/users | updateUser
*VersionresourceApi* | [**addConceptsToVersionUsingPOST**](docs/VersionresourceApi.md#addConceptsToVersionUsingPOST) | **POST** /api/versions/{id}/import | addConceptsToVersion
*VersionresourceApi* | [**createVersionUsingPOST**](docs/VersionresourceApi.md#createVersionUsingPOST) | **POST** /api/versions | createVersion
*VersionresourceApi* | [**deleteVersionUsingDELETE**](docs/VersionresourceApi.md#deleteVersionUsingDELETE) | **DELETE** /api/versions/{id} | deleteVersion
*VersionresourceApi* | [**getAllVersionsUsingGET**](docs/VersionresourceApi.md#getAllVersionsUsingGET) | **GET** /api/versions | getAllVersions
*VersionresourceApi* | [**getConceptsForVersionUsingGET**](docs/VersionresourceApi.md#getConceptsForVersionUsingGET) | **GET** /api/versions/{id}/concepts | getConceptsForVersion
*VersionresourceApi* | [**getDefaultVersionUsingGET**](docs/VersionresourceApi.md#getDefaultVersionUsingGET) | **GET** /api/versions/default | getDefaultVersion
*VersionresourceApi* | [**getVersionUsingGET**](docs/VersionresourceApi.md#getVersionUsingGET) | **GET** /api/versions/{id} | getVersion
*VersionresourceApi* | [**setDefaultVersionUsingPUT**](docs/VersionresourceApi.md#setDefaultVersionUsingPUT) | **PUT** /api/versions/default | setDefaultVersion
*VersionresourceApi* | [**updateVersionUsingPUT**](docs/VersionresourceApi.md#updateVersionUsingPUT) | **PUT** /api/versions | updateVersion


## Documentation for Models

 - [Calendar](docs/Calendar.md)
 - [CallableOfResponseEntityOfSetOfstring](docs/CallableOfResponseEntityOfSetOfstring.md)
 - [CodeSystem](docs/CodeSystem.md)
 - [CollectionOfCalendar](docs/CollectionOfCalendar.md)
 - [CollectionOfExpression](docs/CollectionOfExpression.md)
 - [CollectionOfSnomedConcept](docs/CollectionOfSnomedConcept.md)
 - [CollectionOfSnomedDescription](docs/CollectionOfSnomedDescription.md)
 - [CollectionOfSnomedRelationship](docs/CollectionOfSnomedRelationship.md)
 - [CollectionOfSnomedRelationshipPropertyExpression](docs/CollectionOfSnomedRelationshipPropertyExpression.md)
 - [CollectionOfSnomedRoleGroup](docs/CollectionOfSnomedRoleGroup.md)
 - [CollectionOfSnomedRoleGroupExpression](docs/CollectionOfSnomedRoleGroupExpression.md)
 - [CollectionOfstring](docs/CollectionOfstring.md)
 - [ConstraintValueOfobject](docs/ConstraintValueOfobject.md)
 - [Expression](docs/Expression.md)
 - [KeyAndPasswordDTO](docs/KeyAndPasswordDTO.md)
 - [Locale](docs/Locale.md)
 - [LoginDTO](docs/LoginDTO.md)
 - [ManagedUserDTO](docs/ManagedUserDTO.md)
 - [MapOflongAndMapOfstringAndstring](docs/MapOflongAndMapOfstringAndstring.md)
 - [MapOflongAndstring](docs/MapOflongAndstring.md)
 - [MapOfstringAndstring](docs/MapOfstringAndstring.md)
 - [MetaData](docs/MetaData.md)
 - [NormalFormExpression](docs/NormalFormExpression.md)
 - [Organisation](docs/Organisation.md)
 - [Person](docs/Person.md)
 - [QueryExpression](docs/QueryExpression.md)
 - [Refset](docs/Refset.md)
 - [SnomedConcept](docs/SnomedConcept.md)
 - [SnomedDescription](docs/SnomedDescription.md)
 - [SnomedRelationship](docs/SnomedRelationship.md)
 - [SnomedRelationshipPropertyExpression](docs/SnomedRelationshipPropertyExpression.md)
 - [SnomedRoleGroup](docs/SnomedRoleGroup.md)
 - [SnomedRoleGroupExpression](docs/SnomedRoleGroupExpression.md)
 - [TerminologyConcept](docs/TerminologyConcept.md)
 - [TerminologyConceptImpl](docs/TerminologyConceptImpl.md)
 - [TerminologyConstraint](docs/TerminologyConstraint.md)
 - [TimeZone](docs/TimeZone.md)
 - [URI](docs/URI.md)
 - [UserDTO](docs/UserDTO.md)
 - [Version](docs/Version.md)


## Documentation for Authorization

All endpoints do not require authorization.
Authentication schemes defined for the API:

## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issue.

## Author

info@noesisinformatica.com

